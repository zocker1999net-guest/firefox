# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Utilisar il purschider
    .accesskey = r
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (predefinì)
    .tooltiptext = Utilisar l'URL da standard per resolver DNS via HTTPS
connection-dns-over-https-url-custom =
    .label = Persunalisà
    .accesskey = P
    .tooltiptext = Endatescha l'URL preferì per resolver DNS via HTTPS
connection-dns-over-https-custom-label = Persunalisà
