# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pane-general-title = Powšykne
category-general =
    .tooltiptext = { pane-general-title }
general-language-and-appearance-header = Rěc a wuglěd
general-incoming-mail-header = Dochadajuce mejlki
general-files-and-attachment-header = Dataje a pśidanki
general-tags-header = Wobznamjenja
general-reading-and-display-header = Cytanje a zwobraznjenje
general-updates-header = Aktualizacije
general-network-and-diskspace-header = Seś a platowy rum
general-indexing-label = Indeksěrowanje
composition-category-header = Pisaś
composition-attachments-header = Pśidanki
composition-spelling-title = Pšawopis
compose-html-style-title = HTML-stil
composition-addressing-header = Adresěrowanje
privacy-main-header = Priwatnosć
privacy-passwords-header = Gronidła
privacy-junk-header = Cajk
privacy-data-collection-header = Zběranje a wužywanje datow
privacy-security-header = Wěstota
privacy-scam-detection-title = Nadejźenje wobšudy
privacy-anti-virus-title = Antiwirusowy program
privacy-certificates-title = Certifikaty
chat-pane-header = Chat
chat-status-title = Status
chat-notifications-title = Zdźělenja
chat-pane-styling-header = Formatěrowanje
choose-messenger-language-description = Wubjeŕśo rěcy, kótarež se wužywaju, aby menije, powěsći a powěźeńki z { -brand-short-name } pokazali.
manage-messenger-languages-button =
    .label = Alternatiwy definěrowaś…
    .accesskey = l
confirm-messenger-language-change-description = Startujśo { -brand-short-name } znowego, aby toś te změny nałožył
confirm-messenger-language-change-button = Nałožyś a znowego startowaś
update-pref-write-failure-title = Pisańska zmólka
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = Nastajenje njedajo se składowaś. Njejo było móžno do dataje pisaś: { $path }
update-setting-write-failure-title = Zmólka pśi składowanju aktualizěrowańskich nastajenjow
# Variables:
#   $path (String) - Path to the configuration file
# The newlines between the main text and the line containing the path is
# intentional so the path is easier to identify.
update-setting-write-failure-message =
    { -brand-short-name } jo starcył na zmólku a njejo toś tu změnu składł. Źiwajśo na to, až se toś to aktualizěrowańske nastajenje pisańske pšawo za slědujucu dataju pomina. Wy abo systemowy administrator móžotej zmólku pórěźiś, gaž wužywarskej kupce połnu kontrolu nad toś teju dataju dajotej.
    
    Njedajo se do dataje pisaś: { $path }
update-in-progress-title = Aktualizacija běžy
update-in-progress-message = Cośo, až { -brand-short-name } z toś teju aktualizaciju pókšacujo?
update-in-progress-ok-button = &Zachyśiś
# Continue is the cancel button so pressing escape or using a platform standard
# method of closing the UI will not discard the update.
update-in-progress-cancel-button = &Dalej
