<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Attn: Localization - some of the menus in this dialog directly affect mail also. -->

<!-- File menu items -->

<!-- Edit menu items -->
<!ENTITY pasteNoFormatting.label "Collar sin formato">
<!ENTITY pasteNoFormatting.accesskey "n">
<!ENTITY pasteAs.label "Collar como">
<!ENTITY pasteAs.accesskey "o">
<!ENTITY pasteAsQuotationCmd.label "Collar como citation">
<!ENTITY pasteAsQuotationCmd.accesskey "C">
<!ENTITY pasteAsQuotationCmd.key "i">
<!ENTITY findBarCmd.label "Trovar…">
<!ENTITY findReplaceCmd.label "Trovar e reimplaciar…">
<!ENTITY enableInlineSpellChecker.label "Corrector orthographic durante que tu scribe">
<!ENTITY checkSpellingCmd2.label "Verificar le orthographia…">
<!ENTITY checkSpellingCmd2.accesskey "r">

<!-- Insert menu items -->
<!ENTITY insertMenu.label "Inserer">
<!ENTITY insertMenu.accesskey "I">
<!ENTITY insertLinkCmd2.label "Ligamine…">
<!ENTITY insertLinkCmd2.accesskey "L">
<!ENTITY insertImageCmd.label "Imagine…">
<!ENTITY insertImageCmd.accesskey "I">
<!ENTITY insertHLineCmd.label "Linea horizontal">
<!ENTITY insertHLineCmd.accesskey "o">
<!ENTITY insertTableCmd.label "Tabella…">
<!ENTITY insertTableCmd.accesskey "T">
<!ENTITY insertHTMLCmd.label "HTML…">
<!ENTITY insertHTMLCmd.accesskey "H">
<!ENTITY insertMathCmd.label "Math…">
<!ENTITY insertMathCmd.accesskey "M">
<!ENTITY insertCharsCmd.label "Characteres e symbolos…">
<!ENTITY insertCharsCmd.accesskey "C">

<!-- Used just in context popup. -->
<!ENTITY createLinkCmd.label "Crear ligamine…">
<!ENTITY createLinkCmd.accesskey "g">

<!-- Font Face SubMenu -->
<!ENTITY FontFaceSelect.tooltip "Eliger un typo de character">
<!ENTITY fontfaceMenu.label "Typo de character">
<!ENTITY fontfaceMenu.accesskey "C">
<!ENTITY fontVarWidth.label "Largessa variabile">
<!ENTITY fontVarWidth.accesskey "v">
<!ENTITY fontFixedWidth.label "Largessa fixe">
<!ENTITY fontFixedWidth.accesskey "x">
<!ENTITY fontFixedWidth.key "T">
<!ENTITY fontTimes.label "Tempores">
<!ENTITY fontTimes.accesskey "T">

<!-- Font Size SubMenu -->
<!ENTITY decreaseFontSize.label "Plus micre">
<!ENTITY decreaseFontSize.accesskey "m">
<!ENTITY decrementFontSize.key "&lt;">
<!ENTITY decrementFontSize.key2 ","> <!-- < is above this key on many keyboards -->
<!ENTITY increaseFontSize.label "Plus grande">
<!ENTITY increaseFontSize.accesskey "g">
<!ENTITY incrementFontSize.key "&gt;">
<!ENTITY incrementFontSize.key2 "."> <!-- > is above this key on many keyboards -->

<!ENTITY fontSizeMenu.label "Dimension">
<!ENTITY fontSizeMenu.accesskey "s">
<!ENTITY size-largeCmd.label "Grande">
<!ENTITY size-largeCmd.accesskey "G">

<!-- Font Style SubMenu -->
<!ENTITY styleBoldCmd.label "Hardite">
<!ENTITY styleBoldCmd.accesskey "H">
<!ENTITY styleBoldCmd.key "H">
<!ENTITY styleItalicCmd.label "Italico">
<!ENTITY styleItalicCmd.accesskey "I">
<!ENTITY styleItalicCmd.key "I">
<!ENTITY styleEm.label "Emphase">
<!ENTITY styleEm.accesskey "E">
<!ENTITY styleStrong.label "Emphase plus forte">
<!ENTITY styleStrong.accesskey "t">
<!ENTITY styleCite.label "Citation">
<!ENTITY styleCite.accesskey "C">
<!ENTITY styleAbbr.label "Abbreviation">
<!ENTITY styleAbbr.accesskey "A">
<!ENTITY styleAcronym.label "Acronymo">
<!ENTITY styleAcronym.accesskey "r">
<!ENTITY styleCode.label "Codice">
<!ENTITY styleCode.accesskey "o">
<!ENTITY styleVar.label "Variabile">
<!ENTITY styleVar.accesskey "V">



<!ENTITY paragraphMenu.label "Paragrapho">
<!ENTITY paragraphMenu.accesskey "P">
<!ENTITY paragraphParagraphCmd.label "Paragrapho">
<!ENTITY paragraphParagraphCmd.accesskey "P">
<!ENTITY heading1Cmd.label "Titulo 1">
<!ENTITY heading1Cmd.accesskey "1">
<!ENTITY heading2Cmd.label "Titulo 2">
<!ENTITY heading2Cmd.accesskey "2">
<!ENTITY heading3Cmd.label "Titulo 3">
<!ENTITY heading3Cmd.accesskey "3">
<!ENTITY heading4Cmd.label "Titulo 4">
<!ENTITY heading4Cmd.accesskey "4">
<!ENTITY heading5Cmd.label "Titulo 5">
<!ENTITY heading5Cmd.accesskey "5">
<!ENTITY heading6Cmd.label "Titulo 6">
<!ENTITY heading6Cmd.accesskey "6">
<!ENTITY paragraphAddressCmd.label "Adresse">
<!ENTITY paragraphAddressCmd.accesskey "A">

<!-- List menu items -->
<!ENTITY formatlistMenu.label "Lista">
<!ENTITY formatlistMenu.accesskey "L">
<!ENTITY noneCmd.label "Nulle">
<!ENTITY noneCmd.accesskey "N">
<!ENTITY listNumberedCmd.label "Numerate">
<!ENTITY listNumberedCmd.accesskey "m">
<!ENTITY listTermCmd.label "Termino">
<!ENTITY listTermCmd.accesskey "T">
<!ENTITY listDefinitionCmd.label "Definition">
<!ENTITY listDefinitionCmd.accesskey "D">

<!-- Shared in Paragraph, and Toolbar menulist -->
<!-- isn't used in menu now, but may be added in future -->
<!ENTITY advancedPropertiesCmd.label "Proprietates avantiate">
<!ENTITY advancedPropertiesCmd.accesskey "v">

<!-- Align menu items -->
<!ENTITY alignLeft.label "Sinistra">
<!ENTITY alignLeft.accesskey "S">
<!ENTITY alignLeft.tooltip "Alinear a sinistra">
<!ENTITY alignCenter.label "Centro">
<!ENTITY alignCenter.accesskey "C">
<!ENTITY alignCenter.tooltip "Alinear al centro">
<!ENTITY alignRight.label "Dextera">
<!ENTITY alignRight.accesskey "D">
<!ENTITY alignRight.tooltip "Alinear a dextra">

<!-- Layer toolbar items -->



<!-- Table Menu -->
<!ENTITY tableMenu.label "Tabella">
<!ENTITY tableMenu.accesskey "b">

<!-- Select Submenu -->
<!ENTITY tableSelectMenu.label "Seliger">
<!ENTITY tableSelectMenu.accesskey "S">


<!-- Insert SubMenu -->
<!ENTITY tableInsertMenu.label "Inserer">
<!ENTITY tableInsertMenu.accesskey "I">
<!ENTITY tableTable.label "Tabella">
<!ENTITY tableTable.accesskey "T">
<!ENTITY tableRows.label "Riga(s)">
<!ENTITY tableRow.accesskey "R">
<!ENTITY tableColumn.label "Columna">
<!ENTITY tableColumns.label "Columna(s)">
<!ENTITY tableColumn.accesskey "o">
<!ENTITY tableCell.label "Cella">
<!ENTITY tableCells.label "Cella(s)">
<!ENTITY tableCell.accesskey "C">
<!ENTITY tableCellContents.label "Contento del cella">
<!-- Delete SubMenu -->
<!ENTITY tableDeleteMenu.label "Deler">
<!ENTITY tableDeleteMenu.accesskey "D">

<!-- text for "Join Cells" is in editor.properties
     ("JoinSelectedCells" and "JoinCellToRight")
     the access key must exist in both of those strings
     But value must be set here for accesskey to draw properly
-->

<!-- Toolbar-only items -->
<!ENTITY menuBar.tooltip "Barra de menu">
<!ENTITY cutToolbarCmd.tooltip "Taliar">
<!ENTITY copyToolbarCmd.tooltip "Copiar">
<!ENTITY pasteToolbarCmd.tooltip "Collar">
<!ENTITY printToolbarCmd.label "Imprimer">
<!ENTITY printToolbarCmd.tooltip "Imprimer iste pagina">
<!ENTITY findToolbarCmd.label "Trovar">
<!ENTITY findToolbarCmd.tooltip "Trovar texto in le pagina">
<!ENTITY imageToolbarCmd.label "Imagine">
<!ENTITY tableToolbarCmd.label "Tabella">
<!ENTITY linkToolbarCmd.label "Ligamine">
<!ENTITY anchorToolbarCmd.label "Ancora">

<!-- Editor toolbar -->
<!ENTITY boldToolbarCmd.tooltip "Hardite">
<!ENTITY italicToolbarCmd.tooltip "Italico">

<!-- Structure Toolbar Context Menu items -->
<!ENTITY structSelect.label         "Seliger">
<!ENTITY structSelect.accesskey     "s">
<!ENTITY structRemoveTag.label      "Remover le tag">
<!ENTITY structRemoveTag.accesskey  "r">

<!-- TOC manipulation -->
<!ENTITY insertTOC.label          "Inserer">
<!ENTITY insertTOC.accesskey      "i">
<!ENTITY updateTOC.label          "Actualisar">
<!ENTITY updateTOC.accesskey      "a">
<!ENTITY removeTOC.label          "Remover">
<!ENTITY removeTOC.accesskey      "r">
<!ENTITY tocMenu.label            "Indice del contentos…">
