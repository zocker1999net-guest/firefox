# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-config-warning-checkbox = කරුණාකර, මට නැවත සිහි කැඳවන්න !
about-config-warning-button = මම අවදානම පිළි ගනිමි
about-config2-title = සංකීර්ණ වින්‍යාසයන්
about-config-search-input =
    .placeholder = සොයන්න
about-config-show-all = සියල්ල පෙන්වන්න
about-config-pref-add = එකතු
about-config-pref-toggle = සක්‍රිය කරන්න
about-config-pref-edit = සැකසුම්
about-config-pref-save = සුරකින්න
about-config-pref-reset = යළි සැකසුම
about-config-pref-delete = මකන්න

## Labels for the type selection radio buttons shown when adding preferences.

about-config-pref-add-type-boolean = බූලීය
about-config-pref-add-type-number = අංකය
about-config-pref-add-type-string = පෙළ

## Preferences with a non-default value are differentiated visually, and at the
## same time the state is made accessible to screen readers using an aria-label
## that won't be visible or copied to the clipboard.
##
## Variables:
##   $value (String): The full value of the preference.

about-config-pref-accessible-value-default =
    .aria-label = { $value } (පෙරනිමි)
about-config-pref-accessible-value-custom =
    .aria-label = { $value } (රුචි)
