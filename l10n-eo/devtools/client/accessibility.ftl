# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = Pli da informo
accessibility-text-label-header = Etikedoj kaj nomoj de teksto

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = Averto
accessibility-fail =
    .alt = Eraro
accessibility-best-practices =
    .alt = Plej bonaj praktikoj

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

