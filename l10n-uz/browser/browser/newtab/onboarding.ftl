# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### UI strings for the simplified onboarding modal / about:welcome
### Various strings use a non-breaking space to avoid a single dangling /
### widowed word, so test on various window sizes if you also want this.


## These button action text can be split onto multiple lines, so use explicit
## newlines in translations to control where the line break appears (e.g., to
## avoid breaking quoted text).

onboarding-button-label-try-now = Sinab ko‘ring

## Welcome modal dialog strings

onboarding-welcome-header = { -brand-short-name }ga xush kelibsiz
onboarding-start-browsing-button-label = Internetda kezishni boshlang
onboarding-cards-dismiss =
    .title = Rad etish
    .aria-label = Rad etish

## Firefox Sync modal dialog strings.


## This is part of the line "Enter your email to continue to Firefox Sync"


## These are individual benefit messages shown with an image, title and
## description.


## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Maxfiy ko‘rish
onboarding-private-browsing-text = Internetdan faqat o‘zingiz uchun foydalaning. "Kontentni bloklash" funksiyasi bilan maxfiy ko‘rish rejimida sizni onlayn kuzatishi mumkin bo‘lgan kuzatuvchilarni bloklay olasiz.
onboarding-screenshots-title = Skrinshotlar
onboarding-screenshots-text = { -brand-short-name }dan chiqmasdan skrinshotlar oling, saqlang va ulashing. Xohlaganingizdek butun sahifa yoki bir qismini rasmga olishingiz mumkin. Keyin esa foydalanish va ulashish oson bo‘lishi uchun internetga saqlashingiz mumkin.
onboarding-addons-title = Qo‘shimcha dasturlar
onboarding-addons-text = { -brand-short-name } siz uchun yanada qattiqroq ishlashi uchun yana funksiyalar qo‘shing. Narxlarni solishtirish, ob-havoni tekshirish yoki turli mavzular o‘rnatish kabi imkoniyatini taqdim etadi.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Ghostery kabi kengaytmalar bilan internetda tezroq, oson va xavfsiz ishlash mumkin. U bilan asabga teguvchi reklamalarni bloklash mumkin.

## Message strings belonging to the Return to AMO flow

