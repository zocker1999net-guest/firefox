# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## File Menu

menu-file =
    .label = Yakb'äl
    .accesskey = Y
menu-file-new-tab =
    .label = K'ak'a' ruwi'
    .accesskey = r
menu-file-new-container-tab =
    .label = K'ak'a' k'ojlib'äl ruwi'
    .accesskey = w
menu-file-new-window =
    .label = K'ak'a' tzuwäch
    .accesskey = K
menu-file-new-private-window =
    .label = K'ak'a' ichinan tzuwäch
    .accesskey = t
# "Open Location" is only displayed on macOS, and only on windows
# that aren't main browser windows, or when there are no windows
# but Firefox is still running.
menu-file-open-location =
    .label = Tijaq ri k'ojlemal…
menu-file-open-file =
    .label = Tijaq yakb'äl…
    .accesskey = T
menu-file-close =
    .label = Titz'apïx
    .accesskey = T
menu-file-close-window =
    .label = Titz'apïx Tzuwäch
    .accesskey = w
menu-file-save-page =
    .label = Tiyak ruxaq achi'el…
    .accesskey = a
menu-file-email-link =
    .label = Titaq ximonel tzij…
    .accesskey = T
menu-file-print-setup =
    .label = Tinuk'samajïx ruxaq…
    .accesskey = n
menu-file-print-preview =
    .label = Nab'ey tz'etoj
    .accesskey = e
menu-file-print =
    .label = Titz'ajb'äx…
    .accesskey = T
menu-file-import-from-another-browser =
    .label = Tijik' pa Jun Chik Okik'amaya'l…
    .accesskey = T
menu-file-go-offline =
    .label = Chupül rusamaj
    .accesskey = m

## Edit Menu

menu-edit =
    .label = K'exob'äl
    .accesskey = e
menu-edit-undo =
    .label = Titzolïx
    .accesskey = t
menu-edit-redo =
    .label = Tikamulüx
    .accesskey = m
menu-edit-cut =
    .label = Tiqupïx
    .accesskey = q
menu-edit-copy =
    .label = Tiwachib'ëx
    .accesskey = c
menu-edit-paste =
    .label = Titz'ajb'äx
    .accesskey = T
menu-edit-delete =
    .label = Tiyuj
    .accesskey = T
menu-edit-select-all =
    .label = Ticha' ronojel
    .accesskey = r
menu-edit-find-on =
    .label = Tikanöx pa Re Ruxaq Re'…
    .accesskey = R
menu-edit-find-again =
    .label = Tikanöx Chik
    .accesskey = k
menu-edit-bidi-switch-text-direction =
    .label = Rujalik rub'eyal ri tz'ib'
    .accesskey = r

## View Menu

menu-view =
    .label = Tz'etob'äl
    .accesskey = T
menu-view-toolbars-menu =
    .label = Cholsamajib'äl
    .accesskey = i
menu-view-customize-toolbar =
    .label = Tichinäx…
    .accesskey = T
menu-view-sidebar =
    .label = Chuchi' Cholab'äl
    .accesskey = C
menu-view-bookmarks =
    .label = Yaketal
menu-view-history-button =
    .label = Natab'äl
menu-view-synced-tabs-sidebar =
    .label = Ximon taq ruwi'
menu-view-full-zoom =
    .label = Runimilem
    .accesskey = R
menu-view-full-zoom-enlarge =
    .label = Tinimirisäx
    .accesskey = n
menu-view-full-zoom-reduce =
    .label = Ch'utinarisaxïk
    .accesskey = C
menu-view-full-zoom-reset =
    .label = Tikirib'alil
    .accesskey = T
menu-view-full-zoom-toggle =
    .label = Tinimirisäx xa xe ri tz'ib'
    .accesskey = t
menu-view-page-style-menu =
    .label = Rutzub'al ruxaq
    .accesskey = R
menu-view-page-style-no-style =
    .label = Manäq rutzub'al
    .accesskey = M
menu-view-page-basic-style =
    .label = Rutzub'al ruk'u'x ruxaq
    .accesskey = R
menu-view-charset =
    .label = Rucholajil rucholajem tzij
    .accesskey = c

## These should match what Safari and other Apple applications
## use on macOS.

menu-view-enter-full-screen =
    .label = Yatok pa chijun ruwa kematz'ib'
    .accesskey = c
menu-view-exit-full-screen =
    .label = Tel pa chijun ruwa kematz'ib'
    .accesskey = c
menu-view-full-screen =
    .label = Chijun ruwa kematz'ib'
    .accesskey = C

##

menu-view-show-all-tabs =
    .label = Kek'ut pe ronojel ri taq ruwi'
    .accesskey = t
menu-view-bidi-switch-page-direction =
    .label = Rujalik rub'eyal re ruxaq re'
    .accesskey = r

## History Menu

menu-history =
    .label = Natab'äl
    .accesskey = t
menu-history-show-all-history =
    .label = Tik'ut pe ronojel ri natab'äl
menu-history-clear-recent-history =
    .label = Tiyuj ri k'ak'a' natab'äl…
menu-history-synced-tabs =
    .label = Ximon taq ruwi'
menu-history-restore-last-session =
    .label = Titzolin pa ri molojri'ïl xik'o
menu-history-hidden-tabs =
    .label = Ewan taq Ruwi'
menu-history-undo-menu =
    .label = Taq ruwi' k'a b'a' etz'apin
menu-history-undo-window-menu =
    .label = Taq k'ajtz'ib' k'a b'a' etz'apin

## Bookmarks Menu

menu-bookmarks-menu =
    .label = Yaketal
    .accesskey = y
menu-bookmarks-show-all =
    .label = Kek'ut konojel ri taq yaketal
menu-bookmarks-all-tabs =
    .label = Ketz'aqatisäx taq ruwi' pa taq yaketal…
menu-bookmarks-toolbar =
    .label = Kicholob'al taq kisamajib'al taq yaketal
menu-bookmarks-other =
    .label = Ch'aqa' chik taq Yaketal
menu-bookmarks-mobile =
    .label = Taq ruyaketal oyonib'äl

## Tools Menu

menu-tools =
    .label = Samajib'äl
    .accesskey = i
menu-tools-downloads =
    .label = Taq qasanïk
    .accesskey = q
menu-tools-addons =
    .label = Taq tz'aqat
    .accesskey = t
menu-tools-sync-sign-in =
    .label = Titikirisäx molojri'ïl pa { -sync-brand-short-name }…
    .accesskey = I
menu-tools-sync-now =
    .label = Tixim Wakami
    .accesskey = R
menu-tools-sync-re-auth =
    .label = Tawokisaj chik pa { -sync-brand-short-name }…
    .accesskey = T
menu-tools-web-developer =
    .label = Nuk'unel ajk'amaya'l
    .accesskey = a
menu-tools-page-source =
    .label = Runuk' rutz'ib' re ruxaq re'
    .accesskey = n
menu-tools-page-info =
    .label = Rutzijol rij re jun ruxaq re'
    .accesskey = R
menu-preferences =
    .label =
        { PLATFORM() ->
            [windows] Taq cha'oj
           *[other] Taq Ajowanel
        }
    .accesskey =
        { PLATFORM() ->
            [windows] c
           *[other] o
        }
menu-tools-layout-debugger =
    .label = Ruchojmirisanel Wachib'enïk
    .accesskey = W

## Window Menu

menu-window-menu =
    .label = Window
menu-window-bring-all-to-front =
    .label = Ruk'amik chuwäch ronojel

## Help Menu

menu-help =
    .label = Tob'äl
    .accesskey = T
menu-help-product =
    .label = { -brand-shorter-name } To'ïk
    .accesskey = T
menu-help-show-tour =
    .label = { -brand-shorter-name } B'enam
    .accesskey = n
menu-help-keyboard-shortcuts =
    .label = Ruq'a' rub'ey Keyboard
    .accesskey = K
menu-help-troubleshooting-info =
    .label = Etamab'äl richin yesol taq k'ayewal
    .accesskey = E
menu-help-feedback-page =
    .label = Ketaq taq rutzijol…
    .accesskey = K
menu-help-safe-mode-without-addons =
    .label = Titikirisäx chik kik'in ri chupül taq tz'aqat…
    .accesskey = T
menu-help-safe-mode-with-addons =
    .label = Titikirisäx chik rik'in ri tzijïl taq tz'aqat
    .accesskey = T
# Label of the Help menu item. Either this or
# safeb.palm.notdeceptive.label from
# phishing-afterload-warning-message.dtd is shown.
menu-help-report-deceptive-site =
    .label = Rutzijol q'olonel ruxaq k'amaya'l…
    .accesskey = q
