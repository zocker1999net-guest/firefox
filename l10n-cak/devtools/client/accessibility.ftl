# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = Tetamäx ch'aqa' chik
accessibility-text-label-header = Ketal taq Tz'ib'anïk chuqa' taq B'i'aj

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = K'ayewal
accessibility-fail =
    .alt = Sachoj
accessibility-best-practices =
    .alt = Ütz kib'eyal Samaj

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

accessibility-text-label-issue-area = Tokisäx <code>alt</code> b'anikil richin naya' ketal <div>k'ojlib'äl</div> taq wachinäq kik'wan ri <span>href</span> b'anikil. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-dialog = K'o chi k'o ketal ri ch'owen taq kajtz'ik. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-document-title = Ri taq wujil k'o chi kik'wan jun <code>b'i'aj</code>. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-embed = K'o chi k'o retal ri ch'ikon rupam. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-figure = K'o chi k'o ketal ri cha'el achkib'i' wachib'äl. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-fieldset = <code>fieldset</code> taq wachinäq k'o chi k'o ketal. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-fieldset-legend = Tokisäx <code>legend</code> richin niya' ketal <span>fieldset</span> wachinäq. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-fieldset-legend2 = Tokisäx jun <code>legend</code> wachinäq richin niya' retal jun <span>fieldset</span>. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-form = K'o chi k'o ketal ri kiwachinaq nojwuj. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-form-visible = K'o ta chi wachel rutz'ib'axik ketal kich'akulal nojwuj. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-frame = <code>frame</code> ch'akulal k'o chi ya'on ketal. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-glyph = Tokisäx <code>alt</code> b'anikil richin niya' ketal <span>mglyp</span> taq wachinäq. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-heading = K'o chi k'o ketal ri taq jub'i'aj. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-heading-content = K'o ta chi wachel kitz'ib'anik kipam ri taq jub'i'aj. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-iframe = Tokisäx <code>litle</code> b'anikil richin nichol <span>iframe</span> rupam. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-image = K'o chi k'o retal rupam rik'in wachib'äl. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-interactive = K'o chi k'o ketal ri silonel taq wachinäq. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-optgroup-label = Tokisäx <code>label</code> b'anikil richin niya' ketal <span>optgroup</span> taq wachinäq. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-optgroup-label2 = Tokisäx jun <code>label</code> b'anikil richin niya' retal jun <span>optgroup</span>. <a>Tetamäx ch'aqa' chik</a>
accessibility-text-label-issue-toolbar = K'o chi k'o ketal ri kikajtz'ik samajib'äl toq man xa xe ta jun. <a>Tetamäx ch'aqa' chik</a>
