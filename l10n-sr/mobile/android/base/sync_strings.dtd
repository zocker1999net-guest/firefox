<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Повежи се на &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Да активирате нови уређај, изаберите “Подеси &syncBrand.shortName.label;” на уређају.'>
<!ENTITY sync.subtitle.pair.label 'Да активирате, изаберите “Упари уређај” на другом уређају.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Немам уређај са мном…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Пријаве'>
<!ENTITY sync.configure.engines.title.history 'Историју'>
<!ENTITY sync.configure.engines.title.tabs 'Језичке'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; на &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Мени са забелешкама'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Ознаке'>
<!ENTITY bookmarks.folder.toolbar.label 'Алатна трака забелешки'>
<!ENTITY bookmarks.folder.other.label 'Остале забелешке'>
<!ENTITY bookmarks.folder.desktop.label 'Забелешке радне површине'>
<!ENTITY bookmarks.folder.mobile.label 'Мобилне забелешке'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Закачен'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Назад на прегледање'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Добродошли на &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Пријавите се да бисте синхронизовали језичке, забелешке, пријаве и више.'>
<!ENTITY fxaccount_getting_started_get_started 'Започните'>
<!ENTITY fxaccount_getting_started_old_firefox 'Користите старију &syncBrand.shortName.label; верзију?'>

<!ENTITY fxaccount_status_auth_server 'Налог сервера'>
<!ENTITY fxaccount_status_sync_now 'Синхронизуј сада'>
<!ENTITY fxaccount_status_syncing2 'Синхронизујем…'>
<!ENTITY fxaccount_status_device_name 'Име уређаја'>
<!ENTITY fxaccount_status_sync_server 'Sync сервер'>
<!ENTITY fxaccount_status_needs_verification2 'Налог мора бити потврђен. Притисните да поново пошаљете е-пошту.'>
<!ENTITY fxaccount_status_needs_credentials 'Не могу се повезати. Притисните да се пријавите.'>
<!ENTITY fxaccount_status_needs_upgrade 'Морате да надоградите &brandShortName; да бисте се пријавили.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; је подешен, али не синхронизује аутоматски. Активирајте “Аутоматски синхронизуј податке” у Android поставкама &gt; Коришћење података.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; је подешен, али не синхронизује аутоматски. Активирајте “Аутоматски синхронизуј податке” у Android поставкама &gt; Налози.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Притисните да се пријавите на Firefox налог.'>
<!ENTITY fxaccount_status_choose_what 'Изаберите шта да се синхронизује'>
<!ENTITY fxaccount_status_bookmarks 'Забелешке'>
<!ENTITY fxaccount_status_history 'Историјат'>
<!ENTITY fxaccount_status_passwords2 'Пријаве'>
<!ENTITY fxaccount_status_tabs 'Отворене језичке'>
<!ENTITY fxaccount_status_additional_settings 'Додатне поставке'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Синрхонизуј само путем Wi-Fi'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Спречи &brandShortName; да синхронизује путем мобилног интернета'>
<!ENTITY fxaccount_status_legal 'Права' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Услови коришћења'>
<!ENTITY fxaccount_status_linkprivacy2 'Обавештење о приватности'>
<!ENTITY fxaccount_remove_account 'Прекини&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Прекинути везу са Sync?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Ваши подаци прегледања ће остати на овом уређају, али више неће бити синхронизовани на ваш налог.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Firefox налог &formatS; је уклоњен.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Прекини везу'>

<!ENTITY fxaccount_enable_debug_mode 'Омогући отклањање грешака'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; Опције'>
<!ENTITY fxaccount_options_configure_title 'Конфигурисање &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; није конектован'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Притисните да се пријавите као &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Завршити &syncBrand.shortName.label; надоградњу?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Притисните да се пријавите као &formatS;'>
