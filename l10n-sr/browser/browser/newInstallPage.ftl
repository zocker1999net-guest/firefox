# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### For this feature, "installation" is used to mean "this discrete download of
### Firefox" and "version" is used to mean "the specific revision number of a
### given Firefox channel". These terms are not synonymous.

title = Важне вести
heading = Промене у вашем { -brand-short-name } профилу
changed-title = Шта је промењено?
changed-desc-profiles = Ова инсталација програма { -brand-short-name } користи нови профил. Профил је скуп датотека у којима Firefox чува податке као што су забелешке, лозинке и корисничке поставке.
changed-desc-dedicated = Да бисмо осигурали лакше и безбедније измене између различитих инсталација Firefox-а (укључујући Firefox, Firefox ESR, Firefox Beta, Firefox Developer Edition и Firefox Nightly), ова инсталација сада користи засебан профил. Она не дели самостално ваше сачуване податке са другим Firefox инсталацијама.
lost = <b>Нисте изгубили било шта од ваших личних података и прилагођавања.</b> Ако сте већ сачували податке у Firefox-у на овом рачунару, и даље су ту у тој другој Firefox инсталацији.
options-title = Које су моје опције?
options-do-nothing = Ако не урадите било шта, ваши подаци из профила програма { -brand-short-name } ће бити другачији у односу на податке из профила других Firefox инсталација.
options-use-sync = Ако желите да ваши подаци у профилу буду исти у свим инсталацијама Firefox-а, можете искористити { -fxaccount-brand-name } да би их синхронизовали.
resources = Ресурси:
support-link = Коришћење Управника профила - чланак подршке
sync-header = Пријавите се или направите { -fxaccount-brand-name }
sync-label = Унесите вашу е-адресу
sync-input =
    .placeholder = Е-адреса
sync-button = Настави
sync-terms = Настављањем изражавате да се слажете са <a data-l10n-name="terms">условима услуге</a> и <a data-l10n-name="privacy">политиком приватности</a>.
sync-first = Први пут користите { -sync-brand-name }? Мораћете да се пријавите у свакој инсталацији Firefox-а да бисте синхронизовали ваше податке.
sync-learn = Сазнајте више
