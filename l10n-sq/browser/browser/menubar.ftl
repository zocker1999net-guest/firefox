# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## File Menu

menu-file =
    .label = Kartelë
    .accesskey = K
menu-file-new-tab =
    .label = Skedë e Re
    .accesskey = S
menu-file-new-container-tab =
    .label = Skedë e Re Kontejneri
    .accesskey = o
menu-file-new-window =
    .label = Dritare e Re
    .accesskey = D
menu-file-new-private-window =
    .label = Dritare e Re Private
    .accesskey = P
# "Open Location" is only displayed on macOS, and only on windows
# that aren't main browser windows, or when there are no windows
# but Firefox is still running.
menu-file-open-location =
    .label = Hapni Vendndodhje…
menu-file-open-file =
    .label = Hapni Kartelë…
    .accesskey = K
menu-file-close =
    .label = Mbylleni
    .accesskey = M
menu-file-close-window =
    .label = Mbylleni Dritaren
    .accesskey = D
menu-file-save-page =
    .label = Ruajeni Faqen Si…
    .accesskey = R
menu-file-email-link =
    .label = Dërgoni Lidhje me Email…
    .accesskey = E
menu-file-print-setup =
    .label = Rregullim Faqeje…
    .accesskey = u
menu-file-print-preview =
    .label = Paraparje e Shtypjes
    .accesskey = a
menu-file-print =
    .label = Shtypni…
    .accesskey = y
menu-file-import-from-another-browser =
    .label = Importoni prej Tjetër Shfletuesi…
    .accesskey = I
menu-file-go-offline =
    .label = Puno i Palidhur
    .accesskey = o

## Edit Menu

menu-edit =
    .label = Përpunim
    .accesskey = P
menu-edit-undo =
    .label = Zhbëje
    .accesskey = Z
menu-edit-redo =
    .label = Ribëje
    .accesskey = R
menu-edit-cut =
    .label = Prije
    .accesskey = P
menu-edit-copy =
    .label = Kopjoje
    .accesskey = K
menu-edit-paste =
    .label = Ngjite
    .accesskey = N
menu-edit-delete =
    .label = Fshije
    .accesskey = F
menu-edit-select-all =
    .label = Përzgjidhe Krejt
    .accesskey = P
menu-edit-find-on =
    .label = Gjeni në Këtë Faqe…
    .accesskey = G
menu-edit-find-again =
    .label = Gjeje Sërish
    .accesskey = I
menu-edit-bidi-switch-text-direction =
    .label = Këmbe Drejtim Teksti
    .accesskey = T

## View Menu

menu-view =
    .label = Shfaqje
    .accesskey = S
menu-view-toolbars-menu =
    .label = Panele
    .accesskey = P
menu-view-customize-toolbar =
    .label = Përshtateni…
    .accesskey = P
menu-view-sidebar =
    .label = Anështyllë
    .accesskey = A
menu-view-bookmarks =
    .label = Faqerojtës
menu-view-history-button =
    .label = Historik
menu-view-synced-tabs-sidebar =
    .label = Skeda të Njëkohësuara
menu-view-full-zoom =
    .label = Zmadhim/Zvogëlim
    .accesskey = Z
menu-view-full-zoom-enlarge =
    .label = Zmadhoje
    .accesskey = M
menu-view-full-zoom-reduce =
    .label = Zvogëloje
    .accesskey = O
menu-view-full-zoom-reset =
    .label = Zeroje
    .accesskey = e
menu-view-full-zoom-toggle =
    .label = Zmadho ose Zvogëlo Vetëm Tekstin
    .accesskey = T
menu-view-page-style-menu =
    .label = Stil Faqeje
    .accesskey = S
menu-view-page-style-no-style =
    .label = Pa Stil
    .accesskey = P
menu-view-page-basic-style =
    .label = Stil Bazik Faqeje
    .accesskey = S
menu-view-charset =
    .label = Kodim Teksti
    .accesskey = K

## These should match what Safari and other Apple applications
## use on macOS.

menu-view-enter-full-screen =
    .label = Kaloni nën Sa Krejt Ekrani
    .accesskey = K
menu-view-exit-full-screen =
    .label = Dilni nga Sa Krejt Ekrani
    .accesskey = D
menu-view-full-screen =
    .label = Sa Krejt Ekrani
    .accesskey = E

##

menu-view-show-all-tabs =
    .label = Shfaqini Krejt Skedat
    .accesskey = T
menu-view-bidi-switch-page-direction =
    .label = Këmbe Drejtim Faqeje
    .accesskey = F

## History Menu

menu-history =
    .label = Historik
    .accesskey = H
menu-history-show-all-history =
    .label = Shfaq Krejt Historikun
menu-history-clear-recent-history =
    .label = Pastroni Historikun Së Fundi…
menu-history-synced-tabs =
    .label = Skeda të Njëkohësuara
menu-history-restore-last-session =
    .label = Riktheni Sesionin e Mëparshëm
menu-history-hidden-tabs =
    .label = Skeda të Fshehura
menu-history-undo-menu =
    .label = Skeda të Mbyllura Së Fundi
menu-history-undo-window-menu =
    .label = Dritare të mbyllura Së Fundi

## Bookmarks Menu

menu-bookmarks-menu =
    .label = Faqerojtës
    .accesskey = F
menu-bookmarks-show-all =
    .label = Shfaqni Krejt Faqerojtësit
menu-bookmarks-all-tabs =
    .label = Faqeruani Krejt Skedat…
menu-bookmarks-toolbar =
    .label = Panel Faqerojtësish
menu-bookmarks-other =
    .label = Faqerojtës të Tjerë
menu-bookmarks-mobile =
    .label = Faqerojtës Celulari

## Tools Menu

menu-tools =
    .label = Mjete
    .accesskey = M
menu-tools-downloads =
    .label = Shkarkime
    .accesskey = a
menu-tools-addons =
    .label = Shtesa
    .accesskey = a
menu-tools-sync-sign-in =
    .label = Hyni Te { -sync-brand-short-name }-u…
    .accesskey = H
menu-tools-sync-now =
    .label = Njëkohësohu Tani
    .accesskey = N
menu-tools-sync-re-auth =
    .label = Rilidhuni me { -sync-brand-short-name }-un…
    .accesskey = R
menu-tools-web-developer =
    .label = Zhvillues Web
    .accesskey = Z
menu-tools-page-source =
    .label = Burim Faqeje
    .accesskey = B
menu-tools-page-info =
    .label = Të dhëna Faqeje
    .accesskey = T
menu-preferences =
    .label =
        { PLATFORM() ->
            [windows] Mundësi
           *[other] Parapëlqime
        }
    .accesskey =
        { PLATFORM() ->
            [windows] M
           *[other] a
        }
menu-tools-layout-debugger =
    .label = Diagnostikues Skemash
    .accesskey = D

## Window Menu

menu-window-menu =
    .label = Dritare
menu-window-bring-all-to-front =
    .label = Bjeri të Tëra Para

## Help Menu

menu-help =
    .label = Ndihmë
    .accesskey = N
menu-help-product =
    .label = Ndihmë mbi { -brand-shorter-name }-in
    .accesskey = N
menu-help-show-tour =
    .label = Tur { -brand-shorter-name }-i
    .accesskey = u
menu-help-keyboard-shortcuts =
    .label = Shkurtore Tastiere
    .accesskey = S
menu-help-troubleshooting-info =
    .label = Të dhëna Diagnostikimi
    .accesskey = D
menu-help-feedback-page =
    .label = Parashtroni Përshtypjet…
    .accesskey = P
menu-help-safe-mode-without-addons =
    .label = Riniseni me Shtesat të Çaktivizuara…
    .accesskey = Ç
menu-help-safe-mode-with-addons =
    .label = Rinise me Shtesat të Aktivizuara
    .accesskey = A
# Label of the Help menu item. Either this or
# safeb.palm.notdeceptive.label from
# phishing-afterload-warning-message.dtd is shown.
menu-help-report-deceptive-site =
    .label = Raportoni Sajt të Rremë…
    .accesskey = m
