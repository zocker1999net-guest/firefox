<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  connectionsDialog.title       "Forbindelsesindstillinger">
<!ENTITY  window.width                  "49em">
<!ENTITY  window.macWidth               "44em">

<!ENTITY  proxyTitle.label              "Konfigurer proxy til at tilgå internettet">
<!ENTITY  noProxyTypeRadio.label        "Ingen proxy">
<!ENTITY  noProxyTypeRadio.accesskey    "I">
<!ENTITY  systemTypeRadio.label         "Brug systemets proxy-indstillinger">
<!ENTITY  systemTypeRadio.accesskey     "r">
<!ENTITY  WPADTypeRadio.label           "Auto-detekter proxy-indstillinger for dette netværk">
<!ENTITY  WPADTypeRadio.accesskey       "A">
<!ENTITY  manualTypeRadio.label         "Manuel proxy-konfiguration">
<!ENTITY  manualTypeRadio.accesskey     "M">
<!ENTITY  autoTypeRadio.label           "Automatisk proxy-konfigurations-URL:">
<!ENTITY  autoTypeRadio.accesskey       "U">
<!ENTITY  reload.label                  "Genindlæs">
<!ENTITY  reload.accesskey              "e">
<!ENTITY  http.label                    "HTTP proxy:">
<!ENTITY  http.accesskey                "h">
<!ENTITY  ssl.label                     "SSL proxy">
<!ENTITY  ssl.accesskey                 "s">
<!ENTITY  socks.label                   "SOCKS vært:">
<!ENTITY  socks.accesskey               "c">
<!ENTITY  socks4.label                  "SOCKS v4">
<!ENTITY  socks4.accesskey              "4">
<!ENTITY  socks5.label                  "SOCKS v5">
<!ENTITY  socks5.accesskey              "5">
<!ENTITY  HTTPport.label                "Port:">
<!ENTITY  HTTPport.accesskey            "p">
<!ENTITY  SSLport.label                 "Port:">
<!ENTITY  SSLport.accesskey             "o">
<!ENTITY  SOCKSport.label               "Port:">
<!ENTITY  SOCKSport.accesskey           "t">
<!ENTITY  noproxy.label                 "Ingen proxy for:">
<!ENTITY  noproxy.accesskey             "n">
<!ENTITY  noproxyExplain.label          "Fx .mozilla.org, .net.dk, 192.168.1.0/24">
<!-- LOCALIZATION NOTE (noproxyLocalhostDesc.label): Do not translate
  localhost, 127.0.0.1 and ::1.
-->
<!ENTITY  noproxyLocalhostDesc.label   "Forbindelser til localhost, 127.0.0.1 og ::1 er aldrig forbundet via en proxy.">
<!ENTITY  shareproxy.label              "Brug denne proxy-server for alle protokoller">
<!ENTITY  shareproxy.accesskey          "x">
<!ENTITY  autologinproxy.label          "Spørg ikke efter godkendelse, hvis adgangskoden er gemt">
<!ENTITY  autologinproxy.accesskey      "g">
<!ENTITY  autologinproxy.tooltip        "Denne indstilling godkender dig automatisk overfor proxy-servere, når du har gemt login-informationer til dem. Du bliver spurgt, hvis godkendelsen slår fejl.">
<!ENTITY  socksRemoteDNS.label          "Proxy-DNS ved brug af SOCKS v5">
<!ENTITY  socksRemoteDNS.accesskey      "D">
<!ENTITY  dnsOverHttps.label            "Aktiver DNS via HTTPS">
<!ENTITY  dnsOverHttps.accesskey        "k">
<!ENTITY  dnsOverHttpsUrlDefault.label     "Brug standard">
<!ENTITY  dnsOverHttpsUrlDefault.accesskey "B">
<!ENTITY  dnsOverHttpsUrlDefault.tooltip   "Brug standard-URL'en til DNS-opslag over HTTPS">
<!ENTITY  dnsOverHttpsUrlCustom.label      "Tilpasset">
<!ENTITY  dnsOverHttpsUrlCustom.accesskey  "e">
<!ENTITY  dnsOverHttpsUrlCustom.tooltip    "Angiv den URL, du foretrækker til DNS-opslag over HTTPS">
