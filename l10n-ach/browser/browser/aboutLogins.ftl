# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fxaccounts-sign-in-button = Dony iyie { -sync-brand-short-name }
fxaccounts-avatar-button =
    .title = Lo akaunt

## The ⋯ menu that is in the top corner of the page

menu-menuitem-feedback = Cwal adwogi

## Login List


## Introduction screen


## Login


## Master Password notification


## Dialogs


## Breach Alert notification

