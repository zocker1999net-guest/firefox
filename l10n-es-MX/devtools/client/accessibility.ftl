# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = Más información
accessibility-text-label-header = Etiquetas textuales y nombres

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = Atención
accessibility-fail =
    .alt = Error
accessibility-best-practices =
    .alt = Prácticas idóneas

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

accessibility-text-label-issue-area = Usa el atributo <code>alt</code> para etiquetar elementos <div>area</div> que tengan el atributo <span>href</span>. <a>Saber más</a>
accessibility-text-label-issue-dialog = Los cuadros de diálogo deben etiquetarse. <a>Saber más</a>
accessibility-text-label-issue-document-title = Los documentos deben llevar <code>título</code>. <a>Saber más</a>
accessibility-text-label-issue-embed = El contenido incrustado debe etiquetarse. <a>Saber más información</a>
accessibility-text-label-issue-figure = Las figuras con subtítulos opcionales debieran ser etiquetadas. <a>Saber más más</a>
accessibility-text-label-issue-fieldset = Los elementos <code>fieldset</code> deben ser etiquetados. <a>Saber más</a>
accessibility-text-label-issue-fieldset-legend = Usa el elemento <code>legend</code> para etiquetar elementos <span>fieldset</span>. <a>Saber más</a>
accessibility-text-label-issue-fieldset-legend2 = Usa el elemento <code>legend</code> para etiquetar un <span>fieldset</span>. <a>Saber más</a>
accessibility-text-label-issue-form = Los elementos del formulario deben etiquetarse. <a>Saber más</a>
