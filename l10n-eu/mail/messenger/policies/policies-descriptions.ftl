# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Thunderbird installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-DisableTelemetry = Itzali telemetria.
policy-DNSOverHTTPS = Konfiguratu HTTPS gaineko DNSa.
policy-Proxy = Konfiguratu proxy ezarpenak
policy-SanitizeOnShutdown2 = Garbitu nabigazio datuak itzaltzean.
# For more information, see https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/PKCS11/Module_Installation
policy-SecurityDevices = Instalatu PKCS #11 moduluak.
policy-SSLVersionMax = Ezarri gehienezko SSL bertsioa.
policy-SSLVersionMin = Ezarri gutxieneko SSL bertsioa.
