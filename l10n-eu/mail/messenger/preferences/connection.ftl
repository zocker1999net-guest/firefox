# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Erabili hornitzailea
    .accesskey = h
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name }(lehenetsia)
    .tooltiptext = Erabili URL lehenetsia HTTPSn DNSak ebazteko
connection-dns-over-https-url-custom =
    .label = Pertsonalizatua
    .accesskey = P
    .tooltiptext = Sartu zure gogoko URL HTTPSn DNSak ebazteko
connection-dns-over-https-custom-label = Pertsonalizatua
