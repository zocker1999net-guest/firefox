# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

abuse-report-title-extension = Laporkan Ekstensi Ini ke { -vendor-short-name }
abuse-report-title-theme = Laporkan Tema Ini ke { -vendor-short-name }
abuse-report-subtitle = Apa masalahnya?
# Variables:
#   $author-name (string) - Name of the add-on author
abuse-report-addon-authored-by = oleh <a data-l10n-name="author-name">{ $author-name }</a>
abuse-report-learnmore =
    Tidak yakin masalah apa yang harus dipilih?
    <a data-l10n-name="learnmore-link">Pelajari lebih lanjut tentang melaporkan ekstensi dan tema</a>
abuse-report-submit-description = Jelaskan masalahnya (opsional)

## Panel buttons.

abuse-report-cancel-button = Batal
abuse-report-next-button = Lanjut
abuse-report-goback-button = Kembali
abuse-report-submit-button = Kirim

## Message bars descriptions.


## Variables:
##   $addon-name (string) - Name of the add-on

abuse-report-messagebar-submitted = Terima kasih telah mengirimkan laporan. Apakah Anda ingin menghapus <span data-l10n-name="addon-name">{ $addon-name }</span>?
abuse-report-messagebar-submitted-noremove = Terima kasih telah mengirimkan laporan.
abuse-report-messagebar-removed-extension = Terima kasih telah mengirimkan laporan. Anda telah menghapus ekstensi <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-removed-theme = Terima kasih telah mengirimkan laporan. Anda telah menghapus tema <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-error = Terjadi kesalahan saat mengirim laporan untuk <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-error-recent-submit = Laporan untuk <span data-l10n-name="addon-name">{ $addon-name }</span> tidak terkirim karena laporan lain telah dikirimkan baru-baru ini.

## Message bars actions.

abuse-report-messagebar-action-remove-extension = Ya, Hapus
abuse-report-messagebar-action-remove-theme = Ya, Hapus
abuse-report-messagebar-action-retry = Ulangi
abuse-report-messagebar-action-cancel = Batal

## Abuse report reasons (optionally paired with related examples and/or suggestions)

abuse-report-settings-suggestions-search = Ubah setelan pencarian baku Anda
abuse-report-settings-suggestions-homepage = Ubah beranda dan tab baru Anda
abuse-report-deceptive-reason = Berpura-pura menjadi sesuatu yang palsu
abuse-report-policy-reason = Konten yang berisi kebencian, kekerasan, atau melanggar hukum
abuse-report-unwanted-reason = Tidak pernah menginginkan ekstensi ini dan tidak dapat membuangnya
abuse-report-unwanted-example = Contoh: Sebuah aplikasi memasang sesuatu tanpa izin saya
abuse-report-other-reason = Lainnya
