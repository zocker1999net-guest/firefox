# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = Pelajari lebih lanjut
accessibility-text-label-header = Label dan Nama Teks

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = Peringatan
accessibility-fail =
    .alt = Galat
accessibility-best-practices =
    .alt = Praktik Terbaik

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

accessibility-text-label-issue-dialog = Dialog harus diberi label. <a>Pelajari lebih lanjut</a>
accessibility-text-label-issue-document-title = Dokumen harus memiliki <code>judul</code>. <a>Pelajari lebih lanjut</a>
accessibility-text-label-issue-embed = Konten yang disematkan harus diberi label. <a>Pelajari lebih lanjut</a>
accessibility-text-label-issue-form = Elemen formulir harus diberi label. <a>Pelajari lebih lanjut</a>
accessibility-text-label-issue-form-visible = Elemen formulir harus memiliki label teks yang terlihat. <a>Pelajari lebih lanjut</a>
accessibility-text-label-issue-image = Konten dengan gambar harus diberi label. <a>Pelajari lebih lanjut</a>
accessibility-text-label-issue-interactive = Elemen interaktif harus diberi label. <a>Pelajari lebih lanjut</a>
accessibility-text-label-issue-toolbar = JIka ada lebih dari satu, bilah alat harus diberi label. <a>Pelajari lebih lanjut</a>
