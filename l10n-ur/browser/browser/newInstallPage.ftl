# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### For this feature, "installation" is used to mean "this discrete download of
### Firefox" and "version" is used to mean "the specific revision number of a
### given Firefox channel". These terms are not synonymous.

title = اہم خبر
changed-title = تبدیل کیا ہے
options-title = میرے اختیارات کیا ہیں؟
resources = ماخذ
sync-input =
    .placeholder = ای میل
sync-button = جاری رکھیں
sync-learn = مزید سیکھیں
