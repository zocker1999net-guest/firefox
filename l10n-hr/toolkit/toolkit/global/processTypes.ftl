# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

process-type-web = Web sadržaj
# process used to run privileged pages,
# such as about:home
process-type-privileged = Povlašteni sadržaj
# process used to run privileged about pages,
# such as about:home
process-type-privilegedabout = Povlašteni O nama
# process used to run privileged mozilla pages,
# such as accounts.firefox.com
process-type-privilegedmozilla = Povlašteni Mozilla sadržaj
process-type-extension = Dodatak
# process used to open file:// URLs
process-type-file = Lokalna datoteka
# process used to isolate webpages that requested special
# permission to allocate large amounts of memory
process-type-weblargeallocation = Velika alokacija
# process used to communicate with the GPU for
# graphics acceleration
process-type-gpu = GPU
