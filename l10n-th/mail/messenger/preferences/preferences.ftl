# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = เลือกภาษาที่ใช้แสดงเมนู, ข้อความ และการแจ้งเตือนจาก { -brand-short-name }
manage-messenger-languages-button =
    .label = ตั้งทางเลือก…
    .accesskey = ต
confirm-messenger-language-change-description = เริ่มการทำงาน { -brand-short-name } ใหม่เพื่อใช้การเปลี่ยนแปลงเหล่านี้
confirm-messenger-language-change-button = นำไปใช้แล้วเริ่มการทำงานใหม่
update-pref-write-failure-title = การเขียนล้มเหลว
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = ไม่สามารถบันทึกค่ากำหนด ไม่สามารถเขียนไปยังไฟล์: { $path }
update-in-progress-title = กำลังปรับปรุง
