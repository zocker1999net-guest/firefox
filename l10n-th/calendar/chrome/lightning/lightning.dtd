<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- WARNING! This file contains UTF-8 encoded characters!
   - If this ==> … <== doesn't look like an ellipsis (three dots in a row),
   - your editor isn't using UTF-8 encoding and may munge up the document!
  -->

<!-- Tools menu -->
<!ENTITY lightning.preferencesLabel "ปฏิทิน">

<!-- New menu popup in File menu -->
<!ENTITY lightning.menupopup.new.event.label    "เหตุการณ์…">
<!ENTITY lightning.menupopup.new.event.accesskey "ห">
<!ENTITY lightning.menupopup.new.task.label     "งาน…">
<!ENTITY lightning.menupopup.new.task.accesskey "ง">
<!ENTITY lightning.menupopup.new.calendar.label "ปฏิทิน…">
<!ENTITY lightning.menupopup.new.calendar.accesskey "ป">

<!-- Open menu popup in File menu -->
<!ENTITY lightning.menupopup.open.label               "เปิด">
<!ENTITY lightning.menupopup.open.accesskey           "ป">
<!ENTITY lightning.menupopup.open.message.label       "ข้อความที่บันทึก…">
<!ENTITY lightning.menupopup.open.message.accesskey   "ข">
<!ENTITY lightning.menupopup.open.calendar.label      "ไฟล์ปฏิทิน…">
<!ENTITY lightning.menupopup.open.calendar.accesskey  "ฟ">

<!-- View Menu -->
<!ENTITY lightning.menu.view.calendar.label            "ปฏิทิน">
<!ENTITY lightning.menu.view.calendar.accesskey        "ป">
<!ENTITY lightning.menu.view.tasks.label               "งาน">
<!ENTITY lightning.menu.view.tasks.accesskey           "ง">

<!-- Events and Tasks menu -->
<!ENTITY lightning.menu.eventtask.label                "เหตุการณ์และงาน">
<!ENTITY lightning.menu.eventtask.accesskey            "ห">

<!-- properties dialog, calendar creation wizard -->
<!-- LOCALIZATON NOTE(lightning.calendarproperties.email.label,
     lightning.calendarproperties.forceEmailScheduling.label)
     These strings are used in the calendar wizard and the calendar properties dialog, but are only
     displayed when setting/using a caldav calendar -->
<!ENTITY lightning.calendarproperties.email.label                           "อีเมล:">
<!-- LOCALIZATON NOTE(lightning.calendarproperties.forceEmailScheduling.tooltiptext1,
     lightning.calendarproperties.forceEmailScheduling.tooltiptext2)
     - tooltiptext1 is used in the calendar wizard when setting a new caldav calendar
     - tooltiptext2 is used in the calendar properties dialog for caldav calendars -->

<!-- iMIP Bar (meeting support) -->
<!ENTITY lightning.imipbar.btnAccept.label                                  "ยอมรับ">
<!ENTITY lightning.imipbar.btnAccept2.tooltiptext                           "ยอมรับคำเชิญเหตุการณ์">
<!ENTITY lightning.imipbar.btnAcceptRecurrences.label                       "ยอมรับทั้งหมด">
<!ENTITY lightning.imipbar.btnAdd.label                                     "เพิ่ม">
<!ENTITY lightning.imipbar.btnAdd.tooltiptext                               "เพิ่มเหตุการณ์ไปยังปฏิทิน">
<!ENTITY lightning.imipbar.btnDecline.label                                 "ปฏิเสธ">
<!ENTITY lightning.imipbar.btnDecline2.tooltiptext                          "ปฏิเสธคำเชิญเหตุการณ์">
<!ENTITY lightning.imipbar.btnDeclineRecurrences.label                      "ปฏิเสธทั้งหมด">
<!ENTITY lightning.imipbar.btnDeclineCounter.label                          "ปฏิเสธ">
<!ENTITY lightning.imipbar.btnDeclineCounter.tooltiptext                    "ปฏิเสธการโต้แย้ง">
<!ENTITY lightning.imipbar.btnDelete.label                                  "ลบ">
<!ENTITY lightning.imipbar.btnDelete.tooltiptext                            "ลบออกจากปฏิทิน">
<!ENTITY lightning.imipbar.btnDetails.label                                 "รายละเอียด…">
<!ENTITY lightning.imipbar.btnDetails.tooltiptext                           "แสดงรายละเอียดเหตุการณ์">
<!ENTITY lightning.imipbar.btnMore.label                                    "เพิ่มเติม">
<!ENTITY lightning.imipbar.btnMore.tooltiptext                              "คลิกเพื่อแสดงตัวเลือกเพิ่มเติม">
<!ENTITY lightning.imipbar.btnReconfirm2.label                              "ยืนยันใหม่">
<!ENTITY lightning.imipbar.btnReconfirm.tooltiptext                         "ส่งการยืนยันใหม่ไปยังผู้จัด">
<!ENTITY lightning.imipbar.btnReschedule.label                              "จัดกำหนดการใหม่">
<!ENTITY lightning.imipbar.btnReschedule.tooltiptext                        "จัดกำหนดการเหตุการณ์ใหม่">
<!ENTITY lightning.imipbar.btnSaveCopy.label                                "บันทึกสำเนา">
<!ENTITY lightning.imipbar.btnTentative.label                               "ไม่แน่นอน">
<!ENTITY lightning.imipbar.btnTentative2.tooltiptext                        "ยอมรับคำเชิญเหตุการณ์อย่างไม่แน่นอน">
<!ENTITY lightning.imipbar.btnTentativeRecurrences.label                    "ไม่แน่นอนทั้งหมด">
<!ENTITY lightning.imipbar.btnUpdate.label                                  "อัปเดต">
<!ENTITY lightning.imipbar.btnUpdate.tooltiptext                            "อัปเดตเหตุการณ์ในปฏิทิน">
<!ENTITY lightning.imipbar.description                                      "ข้อความนี้มีคำเชิญไปยังเหตุการณ์">

<!ENTITY lightning.imipbar.btnSend.label                                    "ส่งการตอบสนองตอนนี้">
<!ENTITY lightning.imipbar.btnSend.tooltiptext                              "ส่งการตอบสนองไปยังผู้จัด">
<!ENTITY lightning.imipbar.btnDontSend.label                                "ไม่ส่งการตอบสนอง">

<!-- Lightning specific keybindings -->

<!-- Account Central page -->
<!ENTITY lightning.acctCentral.newCalendar.label "สร้างปฏิทินใหม่">

<!-- today-pane-specific -->
<!ENTITY todaypane.showMinimonth.label "แสดงเดือนขนาดเล็ก">
<!ENTITY todaypane.showMinimonth.accesskey "ส">
<!ENTITY todaypane.showMiniday.label "แสดงวันขนาดเล็ก">
<!ENTITY todaypane.showMiniday.accesskey  "ส">
<!ENTITY todaypane.showNone.label "ไม่แสดง">
<!ENTITY todaypane.showNone.accesskey "ม">
<!ENTITY todaypane.showTodayPane.label "แสดงบานหน้าต่างวันนี้">
<!ENTITY todaypane.showTodayPane.accesskey "ส">
<!ENTITY todaypane.statusButton.label "บานหน้าต่างวันนี้">
