# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = เรียนรู้เพิ่มเติม
accessibility-text-label-header = ป้ายชื่อและชื่อข้อความ

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = คำเตือน
accessibility-fail =
    .alt = ข้อผิดพลาด

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

