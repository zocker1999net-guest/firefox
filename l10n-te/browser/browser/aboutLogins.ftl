# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


# "Google Play" and "App Store" are both branding and should not be translated


## The ⋯ menu that is in the top corner of the page

menu-menuitem-preferences =
    { PLATFORM() ->
        [windows] ఎంపికలు
       *[other] అభిరుచులు
    }

## Login List

login-list-last-changed-option = చివరి మార్పు
login-list-last-used-option = చివరగా వాడినది
login-list-item-subtitle-missing-username = (వాడుకరి పేరు లేదు)

## Introduction screen


## Login

login-item-delete-button = తొలగించు
login-item-username-label = వాడుకరి పేరు
login-item-copy-username-button-text = కాపీచేయి
login-item-copied-username-button-text = కాపీ అయ్యింది!
login-item-password-label = సంకేతపదం
login-item-password-reveal-checkbox-show =
    .title = సంకేతపదం చూపించు
login-item-password-reveal-checkbox-hide =
    .title = సంకేతపదం దాచు
login-item-copy-password-button-text = కాపీచేయి
login-item-copied-password-button-text = కాపీ అయ్యింది!
login-item-save-changes-button = మార్పులను భద్రపరుచు
login-item-save-new-button = భద్రపరుచు
login-item-cancel-button = రద్దుచేయి

## Master Password notification


## Dialogs

confirmation-dialog-cancel-button = రద్దుచేయి
confirmation-dialog-dismiss-button =
    .title = రద్దుచేయి
confirm-delete-dialog-title = ఈ ప్రవేశాన్ని తొలగించాలా?
confirm-delete-dialog-confirm-button = తొలగించు

## Breach Alert notification

