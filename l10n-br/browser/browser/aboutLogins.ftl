# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-logins-page-title = Titouroù kennaskañ

# "Google Play" and "App Store" are both branding and should not be translated

login-app-promo-title = Kemerit ho kerioù-tremen e pep lec'h
login-app-promo-subtitle = Tapit an arload { -lockwise-brand-name } digoust
login-app-promo-android =
    .alt = Tapit anezhañ war Google Play
login-app-promo-apple =
    .alt = Pellgargit anezhañ war an App Store
login-filter =
    .placeholder = Klask titouroù kennaskañ
create-login-button = Krouiñ un titour nevez
# This string is used as alternative text for favicon images.
# Variables:
#   $title (String) - The title of the website associated with the favicon.
login-favicon =
    .alt = Arlun evit { $title }
fxaccounts-sign-in-text = Adkavit ho kerioù-tremen war ho trevnadoù all
fxaccounts-sign-in-button = Kennaskit da { -sync-brand-short-name }
fxaccounts-avatar-button =
    .title = Merañ ar gont

## The ⋯ menu that is in the top corner of the page

menu =
    .title = Digeriñ al lañser
# This menuitem is only visible on Windows
menu-menuitem-import = Enporzhiañ ar gerioù-tremen...
menu-menuitem-preferences =
    { PLATFORM() ->
        [windows] Dibarzhioù
       *[other] Gwellvezioù
    }
menu-menuitem-feedback = Kas hoc'h ali
menu-menuitem-faq = Foar ar goulennoù
menu-menuitem-android-app = { -lockwise-brand-short-name } evit Android
menu-menuitem-iphone-app = { -lockwise-brand-short-name } evit iPhone hag iPad

## Login List

login-list =
    .aria-label = Titouroù kennaskañ a glot gant ar c'hlask
login-list-count =
    { $count ->
        [one] { $count } titour kennaskañ
        [two] { $count } ditour kennaskañ
        [few] { $count } zitour kennaskañ
        [many] { $count } a ditouroù kennaskañ
       *[other] { $count } titour kennaskañ
    }
login-list-sort-label-text = Rummañ dre:
login-list-name-option = Anv (A-Z)
login-list-breached-option = Baradurioù lec'hiennoù
login-list-last-changed-option = Kemmet da ziwezhañ
login-list-last-used-option = Arveret da ziwezhañ
login-list-intro-title = Titour kennaskañ ebet kavet
login-list-intro-description = Pa enrollit ur ger-tremen e { -brand-product-name } e vo diskouezet amañ
login-list-item-title-new-login = Titour kennaskañ nevez
login-list-item-subtitle-new-login = Enankit ho titouroù kennaskañ
login-list-item-subtitle-missing-username = (anv arveriad ebet)

## Introduction screen

login-intro-heading = Klask a rit ho titouroù kennaskañ? Arventennit { -sync-brand-short-name }.
login-intro-description = M'ho peus enrollet ho titouroù kennaskañ { -brand-product-name } war un trevnad all, setu penaos kaout anezho amañ:
login-intro-instruction-fxa = Krouit pe kennaskit d'ho { -fxaccount-brand-name } war an trevnad lec'h m'eo enrollet ho titouroù kennaskañ

## Login


## Master Password notification


## Dialogs


## Breach Alert notification

