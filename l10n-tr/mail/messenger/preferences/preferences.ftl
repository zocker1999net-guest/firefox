# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = { -brand-short-name } menülerini, iletilerini ve bildirimlerini göstermede kullanılacak dilleri seçin.
manage-messenger-languages-button =
    .label = Alternatifleri ayarla…
    .accesskey = l
confirm-messenger-language-change-description = Bu değişiklikleri uygulamak için { -brand-short-name } yeniden başlatılmalıdır
confirm-messenger-language-change-button = Uygula ve yeniden başlat
update-pref-write-failure-title = Yazma Hatası
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = Tercih kaydedilemiyor. Dosyaya yazılamadı: { $path }
update-setting-write-failure-title = Güncelleme tercihleri kaydedilirken hata oluştu
update-in-progress-title = Güncelleme sürüyor
