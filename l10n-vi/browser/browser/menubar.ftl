# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## File Menu

menu-file =
    .label = Tập tin
    .accesskey = F
menu-file-new-tab =
    .label = Thẻ mới
    .accesskey = T
menu-file-new-container-tab =
    .label = Thẻ ngăn chứa mới
    .accesskey = B
menu-file-new-window =
    .label = Cửa sổ mới
    .accesskey = N
menu-file-new-private-window =
    .label = Cửa sổ riêng tư mới
    .accesskey = W
# "Open Location" is only displayed on macOS, and only on windows
# that aren't main browser windows, or when there are no windows
# but Firefox is still running.
menu-file-open-location =
    .label = Mở địa chỉ…
menu-file-open-file =
    .label = Mở tập tin…
    .accesskey = O
menu-file-close =
    .label = Đóng
    .accesskey = C
menu-file-close-window =
    .label = Đóng cửa sổ
    .accesskey = d
menu-file-save-page =
    .label = Lưu trang dưới dạng…
    .accesskey = A
menu-file-email-link =
    .label = Gửi liên kết qua email…
    .accesskey = E
menu-file-print-setup =
    .label = Thiết lập trang…
    .accesskey = u
menu-file-print-preview =
    .label = Xem trước khi in
    .accesskey = v
menu-file-print =
    .label = In…
    .accesskey = P
menu-file-import-from-another-browser =
    .label = Nhập dữ liệu từ trình duyệt khác…
    .accesskey = I
menu-file-go-offline =
    .label = Làm việc ngoại tuyến
    .accesskey = k

## Edit Menu

menu-edit =
    .label = Chỉnh sửa
    .accesskey = E
menu-edit-undo =
    .label = Hoàn tác
    .accesskey = U
menu-edit-redo =
    .label = Làm lại
    .accesskey = R
menu-edit-cut =
    .label = Cắt
    .accesskey = t
menu-edit-copy =
    .label = Sao chép
    .accesskey = C
menu-edit-paste =
    .label = Dán
    .accesskey = P
menu-edit-delete =
    .label = Xóa
    .accesskey = D
menu-edit-select-all =
    .label = Chọn tất cả
    .accesskey = A
menu-edit-find-on =
    .label = Tìm trong trang này…
    .accesskey = F
menu-edit-find-again =
    .label = Tìm lại
    .accesskey = g
menu-edit-bidi-switch-text-direction =
    .label = Chuyển hướng văn bản
    .accesskey = w

## View Menu

menu-view =
    .label = Hiển thị
    .accesskey = V
menu-view-toolbars-menu =
    .label = Thanh công cụ
    .accesskey = T
menu-view-customize-toolbar =
    .label = Tùy biến…
    .accesskey = C
menu-view-sidebar =
    .label = Thanh bên
    .accesskey = e
menu-view-bookmarks =
    .label = Trang đánh dấu
menu-view-history-button =
    .label = Lịch sử
menu-view-synced-tabs-sidebar =
    .label = Các thẻ đã đồng bộ
menu-view-full-zoom =
    .label = Thu phóng
    .accesskey = Z
menu-view-full-zoom-enlarge =
    .label = Phóng to
    .accesskey = I
menu-view-full-zoom-reduce =
    .label = Thu nhỏ
    .accesskey = O
menu-view-full-zoom-reset =
    .label = Đặt lại
    .accesskey = R
menu-view-full-zoom-toggle =
    .label = Chỉ phóng to văn bản
    .accesskey = T
menu-view-page-style-menu =
    .label = Kiểu của trang
    .accesskey = y
menu-view-page-style-no-style =
    .label = Không có kiểu
    .accesskey = n
menu-view-page-basic-style =
    .label = Kiểu trang cơ bản
    .accesskey = b
menu-view-charset =
    .label = Bảng mã văn bản
    .accesskey = c

## These should match what Safari and other Apple applications
## use on macOS.

menu-view-enter-full-screen =
    .label = Vào chế độ toàn màn hình
    .accesskey = F
menu-view-exit-full-screen =
    .label = Thoát chế độ toàn màn hình
    .accesskey = F
menu-view-full-screen =
    .label = Toàn màn hình
    .accesskey = F

##

menu-view-show-all-tabs =
    .label = Hiện tất cả các thẻ
    .accesskey = A
menu-view-bidi-switch-page-direction =
    .label = Chuyển hướng trang
    .accesskey = D

## History Menu

menu-history =
    .label = Lịch sử
    .accesskey = s
menu-history-show-all-history =
    .label = Xem toàn bộ lịch sử
menu-history-clear-recent-history =
    .label = Xóa lịch sử gần đây…
menu-history-synced-tabs =
    .label = Các thẻ đã đồng bộ
menu-history-restore-last-session =
    .label = Khôi phục phiên làm việc trước
menu-history-hidden-tabs =
    .label = Thẻ đã ẩn
menu-history-undo-menu =
    .label = Thẻ mới đóng gần đây
menu-history-undo-window-menu =
    .label = Các cửa sổ mới đóng

## Bookmarks Menu

menu-bookmarks-menu =
    .label = Trang đánh dấu
    .accesskey = B
menu-bookmarks-show-all =
    .label = Xem tất cả trang đánh dấu
menu-bookmarks-all-tabs =
    .label = Đánh dấu tất cả các thẻ…
menu-bookmarks-toolbar =
    .label = Thanh đánh dấu
menu-bookmarks-other =
    .label = Trang đánh dấu khác
menu-bookmarks-mobile =
    .label = Trang đánh dấu trên di động

## Tools Menu

menu-tools =
    .label = Công cụ
    .accesskey = T
menu-tools-downloads =
    .label = Tải xuống
    .accesskey = D
menu-tools-addons =
    .label = Tiện ích
    .accesskey = A
menu-tools-sync-sign-in =
    .label = Đăng nhập vào { -sync-brand-short-name }…
    .accesskey = Y
menu-tools-sync-now =
    .label = Đồng bộ ngay
    .accesskey = S
menu-tools-sync-re-auth =
    .label = Kết nối lại với { -sync-brand-short-name }…
    .accesskey = R
menu-tools-web-developer =
    .label = Nhà phát triển Web
    .accesskey = W
menu-tools-page-source =
    .label = Mở mã nguồn trang
    .accesskey = o
menu-tools-page-info =
    .label = Thông tin về trang này
    .accesskey = I
menu-preferences =
    .label =
        { PLATFORM() ->
            [windows] Tùy chọn
           *[other] Tùy chỉnh
        }
    .accesskey =
        { PLATFORM() ->
            [windows] O
           *[other] n
        }
menu-tools-layout-debugger =
    .label = Trình gỡ lỗi bố cục
    .accesskey = L

## Window Menu

menu-window-menu =
    .label = Cửa sổ
menu-window-bring-all-to-front =
    .label = Đưa tất cả ra phía trước

## Help Menu

menu-help =
    .label = Trợ giúp
    .accesskey = H
menu-help-product =
    .label = Trợ giúp { -brand-shorter-name }
    .accesskey = H
menu-help-show-tour =
    .label = Các tính năng cơ bản của { -brand-shorter-name }
    .accesskey = o
menu-help-keyboard-shortcuts =
    .label = Các phím tắt bàn phím
    .accesskey = K
menu-help-troubleshooting-info =
    .label = Thông tin gỡ rối vấn đề
    .accesskey = T
menu-help-feedback-page =
    .label = Gửi phản hồi…
    .accesskey = S
menu-help-safe-mode-without-addons =
    .label = Khởi động lại và vô hiệu hóa các tiện ích…
    .accesskey = R
menu-help-safe-mode-with-addons =
    .label = Khởi động lại và kích hoạt các tiện ích
    .accesskey = R
# Label of the Help menu item. Either this or
# safeb.palm.notdeceptive.label from
# phishing-afterload-warning-message.dtd is shown.
menu-help-report-deceptive-site =
    .label = Báo cáo trang lừa đảo…
    .accesskey = c
