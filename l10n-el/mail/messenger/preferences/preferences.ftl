# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pane-general-title = Γενικά
category-general =
    .tooltiptext = { pane-general-title }
general-language-and-appearance-header = Γλώσσα & εμφάνιση
general-incoming-mail-header = Εισερχόμενα email
general-files-and-attachment-header = Αρχεία & συνημμένα
general-tags-header = Ετικέτες
general-reading-and-display-header = Ανάγνωση & προβολή
general-updates-header = Ενημερώσεις
general-network-and-diskspace-header = Δίκτυο & χώρος δίσκου
general-indexing-label = Ευρετηριοποίηση
composition-category-header = Σύνθεση
composition-attachments-header = Συνημμένα
composition-spelling-title = Ορθογραφία
compose-html-style-title = Στυλ HTML
composition-addressing-header = Διευθυνσιοδότηση
privacy-main-header = Απόρρητο
privacy-passwords-header = Κωδικοί πρόσβασης
privacy-junk-header = Ανεπιθύμητα
privacy-data-collection-header = Συλλογή και χρήση δεδομένων
privacy-security-header = Ασφάλεια
privacy-scam-detection-title = Ανίχνευση απάτης
privacy-anti-virus-title = Προστασία από ιούς
privacy-certificates-title = Πιστοποιητικά
chat-pane-header = Συνομιλία
chat-status-title = Κατάσταση
chat-notifications-title = Ειδοποιήσεις
chat-pane-styling-header = Στυλ
choose-messenger-language-description = Επιλέξτε τις γλώσσες εμφάνισης για μενού, μηνύματα και ειδοποιήσεις από το { -brand-short-name }.
manage-messenger-languages-button =
    .label = Ορισμός εναλλακτικών...
    .accesskey = λ
confirm-messenger-language-change-description = Επανεκκίνηση του { -brand-short-name } για εφαρμογή αλλαγών
confirm-messenger-language-change-button = Εφαρμογή και επανεκκίνηση
update-pref-write-failure-title = Αποτυχία εγγραφής
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = Αδυναμία αποθήκευσης προτίμησης. Αδυναμία εγγραφής σε αρχείο: { $path }
update-setting-write-failure-title = Σφάλμα αποθήκευσης προτιμήσεων ενημερώσεων
# Variables:
#   $path (String) - Path to the configuration file
# The newlines between the main text and the line containing the path is
# intentional so the path is easier to identify.
update-setting-write-failure-message =
    Το { -brand-short-name } αντιμετώπισε σφάλμα και δεν αποθήκευσε αυτή την αλλαγή. Σημειώστε ότι η ρύθμιση αυτής της προτίμησης ενημερώσεων απαιτεί δικαίωμα εγγραφής στο παρακάτω αρχείο. Εσείς ή κάποιος διαχειριστής συστήματος ενδέχεται να μπορέσει να επιλύσει το σφάλμα, χορηγώντας στην ομάδα Χρήστες τον πλήρη έλεγχο για αυτό το αρχείο.
    
    Αδυναμία εγγραφής στο αρχείο: { $path }
update-in-progress-title = Ενημέρωση σε εξέλιξη
update-in-progress-message = Θέλετε το { -brand-short-name } να συνεχίσει με αυτή την ενημέρωση;
update-in-progress-ok-button = &Απόρριψη
# Continue is the cancel button so pressing escape or using a platform standard
# method of closing the UI will not discard the update.
update-in-progress-cancel-button = &Συνέχεια
