# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### UI strings for the simplified onboarding modal / about:welcome
### Various strings use a non-breaking space to avoid a single dangling /
### widowed word, so test on various window sizes if you also want this.


## These button action text can be split onto multiple lines, so use explicit
## newlines in translations to control where the line break appears (e.g., to
## avoid breaking quoted text).

onboarding-button-label-try-now = Cubalah Sekarang
onboarding-button-label-get-started = Mulakan

## Welcome modal dialog strings

onboarding-welcome-header = Selamat datang ke { -brand-short-name }
onboarding-start-browsing-button-label = Mulakan Pelayaran
onboarding-cards-dismiss =
    .title = Abai
    .aria-label = Abai

## Firefox Sync modal dialog strings.

onboarding-sync-welcome-header = Bawa { -brand-product-name } bersama Anda
onboarding-sync-welcome-content = Dapatkan tandabuku, sejarah, kata laluan dan tetapan lain dalam semua peranti anda.
onboarding-sync-welcome-learn-more-link = Ketahui selanjutnya perihal Akaun Firefox
onboarding-sync-form-invalid-input = Perlu e-mel yang sah
onboarding-sync-legal-notice = Dengan meneruskan, anda bersetuju dengan <a data-l10n-name="terms">Terma Perkhidmatan</a> dan <a data-l10n-name="privacy">Notis Privasi</a>.
onboarding-sync-form-input =
    .placeholder = E-mel
onboarding-sync-form-continue-button = Teruskan
onboarding-sync-form-skip-login-button = Langkau langkah ini

## This is part of the line "Enter your email to continue to Firefox Sync"

onboarding-sync-form-header = Masukkan e-mel anda
onboarding-sync-form-sub-header = untuk ke { -sync-brand-name }

## These are individual benefit messages shown with an image, title and
## description.


## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Pelayaran Peribadi
onboarding-private-browsing-text = Layar sendirian. Pelayaran Peribadi dengan Sekatan Kandungan menyekat penjejak atas talian yang menjejaki anda dalam seluruh web.
onboarding-screenshots-title = Screenshots
onboarding-screenshots-text = Ambil, simpan dan kongsi skrinsyot — tanpa keluar daripada { -brand-short-name }. Rakam gambar sebahagian atau keseluruhan halaman sambil melayari web. Kemudian simpan dalam web supaya mudah diiakses dan dikongsi.
onboarding-addons-title = Add-ons
onboarding-addons-text = Add-ons membolehkan anda menambah ciri { -brand-short-name } supaya bekerja lebih keras untuk anda. Bandingkan harga, menyemak cuaca atau menggambarkan personaliti anda dengan tema tersuai.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Pelayaran lebih cepat, lebih bijak atau lebih selamat dengan ekstensi seperti Ghostery, yang membolehkan anda menyekat iklan yang menjengkelkan.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Sync
onboarding-fxa-text = Daftar dengan { -fxaccount-brand-name } dan selaraskan tandabuku, kata laluan dan tab terbuka di mana sahaja anda menggunakan { -brand-short-name }.

## Message strings belonging to the Return to AMO flow

