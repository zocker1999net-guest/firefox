# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### UI strings for the simplified onboarding modal / about:welcome
### Various strings use a non-breaking space to avoid a single dangling /
### widowed word, so test on various window sizes if you also want this.


## These button action text can be split onto multiple lines, so use explicit
## newlines in translations to control where the line break appears (e.g., to
## avoid breaking quoted text).

onboarding-button-label-try-now = Enndu ɗum jooni
onboarding-button-label-get-started = Fuɗɗo

## Welcome modal dialog strings

onboarding-welcome-header = A jaɓɓaama e { -brand-short-name }
onboarding-start-browsing-button-label = Fuɗɗo wanngaade
onboarding-cards-dismiss =
    .title = Salo
    .aria-label = Salo

## Firefox Sync modal dialog strings.

onboarding-sync-welcome-header = Nawor { -brand-product-name }
onboarding-sync-welcome-content = Heɓ maantore maa, aslol maa, finndeeji maa kam e teelte goɗɗe e masiŋon maa fof.
onboarding-sync-welcome-learn-more-link = Ɓeydu humpito baɗte Konte Firefox
onboarding-sync-form-invalid-input = Iimeel gollotooɗo hatojinaa
onboarding-sync-legal-notice = So a waɗii ɗum, firti ko a jaɓii <a data-l10n-name="terms">Laabi Carwol</a> e <a data-l10n-name="privacy">Tintinol Suturo</a>.
onboarding-sync-form-input =
    .placeholder = Iimeel
onboarding-sync-form-continue-button = Jokku
onboarding-sync-form-skip-login-button = Diw ngal daawal

## This is part of the line "Enter your email to continue to Firefox Sync"

onboarding-sync-form-header = Naatnu iimeel maa
onboarding-sync-form-sub-header = ngam jokkude to { -sync-brand-name }

## These are individual benefit messages shown with an image, title and
## description.


## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Banngogol Suturo
onboarding-private-browsing-text = Wanngano hoore maa. Banngagol sutura paddotoongol ina faddoo rewindo e ceŋol rewindiiɗi ma nder geese.
onboarding-screenshots-title = Leƴƴanɗe kuurga
onboarding-screenshots-text = Ƴettu, kisnaa, lollinaa natte yaynirde - tawa a yaltaani { -brand-short-name }. Natto diiwaan walla hello huurngo so aɗa wanngoo. Caggal ɗuum hisnu nder geese ngam newnude gartol e lollingol.
onboarding-addons-title = ɓeyditte
onboarding-addons-text = Ɓeydu kadi gollirɗe goɗɗe mbele { -brand-short-name } ina gollano maa no moƴƴi. Yerondir coodguuli, humpito meteyoo walla hollitir neɗɗaagu maa kettol neɗɗinangol.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Huutoro njokkon mbaakon no Ghostery ngam banngagol ɓurngol yaawde, ɓurngol ƴoƴde walla siirde, sibu ekon paddoo jeeyle kaljinooje.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Sync
onboarding-fxa-text = Winndito ngam { -fxaccount-brand-name } njahdinaa maante maa, pinle maa kam e tabbe udditiiɗe kala ɗo kuutorto-ɗaa { -brand-short-name }.

## Message strings belonging to the Return to AMO flow

