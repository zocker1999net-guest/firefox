# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Použiť poskytovateľa
    .accesskey = s
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (predvolený)
    .tooltiptext = Použiť predvolenú URL pre DNS cez HTTPS
connection-dns-over-https-url-custom =
    .label = Vlastný
    .accesskey = V
    .tooltiptext = Zadajte svoju preferovanú URL pre DNS cez HTTPS
connection-dns-over-https-custom-label = Vlastný
