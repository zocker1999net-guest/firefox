# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pane-general-title = Všeobecné
category-general =
    .tooltiptext = { pane-general-title }
general-language-and-appearance-header = Jazyk a vzhľad stránok
general-incoming-mail-header = Doručená pošta
general-files-and-attachment-header = Súbory a prílohy
general-tags-header = Značky
general-reading-and-display-header = Čítanie a zobrazenie
general-updates-header = Aktualizácie
general-network-and-diskspace-header = Sieť a miesto na disku
general-indexing-label = Indexovanie
composition-category-header = Písanie správ
composition-attachments-header = Prílohy
composition-spelling-title = Pravopis
compose-html-style-title = Štýl HTML
composition-addressing-header = Adresovanie
privacy-main-header = Súkromie
privacy-passwords-header = Heslá
privacy-junk-header = Spam
privacy-data-collection-header = Zbieranie údajov a ich použitie
privacy-security-header = Bezpečnosť
privacy-scam-detection-title = Detekcia podvodov
privacy-anti-virus-title = Antivírus
privacy-certificates-title = Certifikáty
chat-pane-header = Konverzácie
chat-status-title = Stav
chat-notifications-title = Upozornenia
chat-pane-styling-header = Štylizovanie
choose-messenger-language-description = Vyberte si jazyk, v ktorom sa majú zobrazovať ponuky, správy a oznámenia aplikácie { -brand-short-name }.
manage-messenger-languages-button =
    .label = Vybrať alternatívy…
    .accesskey = a
confirm-messenger-language-change-description = Ak chcete použiť tieto zmeny, reštartujte { -brand-short-name }
confirm-messenger-language-change-button = Použiť a reštartovať
update-pref-write-failure-title = Chyba pri zápise
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = Nepodarilo sa nám uložiť nastavenie. Nebolo možné zapísať údaje do súboru { $path }
update-setting-write-failure-title = Chyba pri ukladaní nastavení aktualizácií
# Variables:
#   $path (String) - Path to the configuration file
# The newlines between the main text and the line containing the path is
# intentional so the path is easier to identify.
update-setting-write-failure-message =
    Aplikácia { -brand-short-name } sa stretla s chybou a túto zmenu neuložila. Berte na vedomie, že upravenie tejto možnosti vyžaduje povolenie na zápis do tohto súboru. Vy alebo váš správca systému môžete túto chybu vyriešiť udelením správnych povolení.
    
    Nebolo možné zapísať do súboru: { $path }
update-in-progress-title = Prebieha aktualizácia
update-in-progress-message = Chcete, aby { -brand-short-name } pokračoval v tejto aktualizácii?
update-in-progress-ok-button = &Zrušiť
# Continue is the cancel button so pressing escape or using a platform standard
# method of closing the UI will not discard the update.
update-in-progress-cancel-button = &Pokračovať
