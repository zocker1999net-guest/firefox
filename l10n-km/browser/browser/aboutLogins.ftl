# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-logins-page-title = ការចូល និង​ពាក្យ​សម្ងាត់

# "Google Play" and "App Store" are both branding and should not be translated

login-app-promo-title = នាំ​យក​ពាក្យ​សម្ងាត់​របស់​អ្នក​គ្រប់​ទីកន្លែង
login-app-promo-subtitle = ទាញយក​កម្មវិធី { -lockwise-brand-name } ឥត​គិតថ្លៃ
login-app-promo-android =
    .alt = ទាញយក​កម្មវិធី​នៅ​លើ Google Play
login-app-promo-apple =
    .alt = ទាញយក​នៅ​លើ App Store
login-filter =
    .placeholder = ស្វែងរក​ការចូល
create-login-button = បង្កើត​ការចូល​ថ្មី
# This string is used as alternative text for favicon images.
# Variables:
#   $title (String) - The title of the website associated with the favicon.
login-favicon =
    .alt = រូបតំណាង​សំណព្វ​សម្រាប់ { $title }
fxaccounts-sign-in-text = ទាញយក​ពាក្យ​សម្ងាត់​របស់​អ្នក​នៅ​លើ​ឧបករណ៍​ផ្សេងៗ​របស់​អ្នក
fxaccounts-sign-in-button = ចូល​ទៅ { -sync-brand-short-name }
fxaccounts-avatar-button =
    .title = គ្រប់គ្រង​គណនី

## The ⋯ menu that is in the top corner of the page

menu =
    .title = បើក​ម៉ឺនុយ
# This menuitem is only visible on Windows
menu-menuitem-import = នាំចូល​ពាក្យ​សម្ងាត់...
menu-menuitem-preferences =
    { PLATFORM() ->
        [windows] ជម្រើស
       *[other] ចំណូលចិត្ត
    }
menu-menuitem-feedback = ផ្ញើ​មតិ​កែលម្អ
menu-menuitem-faq = សំណួរ​ដែល​សួរ​ញឹកញាប់
menu-menuitem-android-app = { -lockwise-brand-short-name } សម្រាប់ Android
menu-menuitem-iphone-app = { -lockwise-brand-short-name } សម្រាប់ iPhone និង iPad

## Login List

login-list =
    .aria-label = ការចូល​ត្រូវ​គ្នា​ជាមួយ​សំណួរ​ស្វែងរក
login-list-count =
    { $count ->
       *[other] ការចូល​ចំនួន { $count }
    }
login-list-sort-label-text = តម្រៀបតាម៖
login-list-name-option = ឈ្មោះ (A-Z)
login-list-last-changed-option = បាន​កែប្រែ​ចុងក្រោយ
login-list-last-used-option = បាន​ប្រើប្រាស់​ចុងក្រោយ
login-list-intro-title = រក​មិន​ឃើញ​ការចូល​ទេ
login-list-intro-description = នៅពេល​អ្នក​រក្សាទុក​ពាក្យ​សម្ងាត់​នៅ​ក្នុង { -brand-product-name } វា​នឹង​បង្ហាញ​នៅ​ត្រង់​នេះ។
login-list-item-title-new-login = ការចូល​ថ្មី
login-list-item-subtitle-new-login = បញ្ចូល​ព័ត៌មាន​លម្អិត​ការចូល​របស់​អ្នក
login-list-item-subtitle-missing-username = (គ្មាន​ឈ្មោះ​អ្នក​ប្រើប្រាស់)

## Introduction screen

login-intro-heading = កំពុង​រក​មើល​ការចូល​ដែល​បាន​រក្សាទុក​របស់​អ្នក​មែន​ទេ? រៀបចំ { -sync-brand-short-name } ។
login-intro-description = ប្រសិនបើ​អ្នក​បាន​រក្សាទុក​ការចូល​របស់​អ្នក​ទៅ { -brand-product-name } នៅ​លើ​ឧបករណ៍​ផ្សេង នេះជា​របៀប​​ចូល​​មើល​ការចូល​ទាំងនោះ​នៅ​ត្រង់នេះ៖

## Login


## Master Password notification


## Dialogs


## Breach Alert notification

