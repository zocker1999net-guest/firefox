# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## File Menu

menu-file =
    .label = ឯកសារ
    .accesskey = F
menu-file-new-tab =
    .label = ផ្ទាំង​ថ្មី
    .accesskey = T
menu-file-new-container-tab =
    .label = ផ្ទាំង​ឧបករណ៍​ផ្ទុក​ថ្មី
    .accesskey = B
menu-file-new-window =
    .label = បង្អួច​​​ថ្មី
    .accesskey = N
menu-file-new-private-window =
    .label = បង្អួច​ឯកជន​ថ្មី
    .accesskey = W
# "Open Location" is only displayed on macOS, and only on windows
# that aren't main browser windows, or when there are no windows
# but Firefox is still running.
menu-file-open-location =
    .label = បើក​ទីតាំង…
menu-file-open-file =
    .label = បើក​ឯកសារ…
    .accesskey = O
menu-file-close =
    .label = បិទ
    .accesskey = C
menu-file-close-window =
    .label = បិទបង្អួច
    .accesskey = d
menu-file-save-page =
    .label = រក្សា​ទុក​ទំព័រជា...
    .accesskey = A
menu-file-email-link =
    .label = តំណ​អ៊ីមែល…
    .accesskey = E
menu-file-print-setup =
    .label = ការ​រៀបចំ​ទំព័រ…
    .accesskey = u
menu-file-print-preview =
    .label = មើល​មុន​បោះពុម្ព
    .accesskey = v
menu-file-print =
    .label = បោះពុម្ព…
    .accesskey = P
menu-file-go-offline =
    .label = ធ្វើ​ការដោយ​គ្មាន​អ៊ីនធឺណិត
    .accesskey = k

## Edit Menu

menu-edit =
    .label = កែសម្រួល
    .accesskey = E
menu-edit-undo =
    .label = មិនធ្វើវិញ
    .accesskey = U
menu-edit-redo =
    .label = ធ្វើវិញ
    .accesskey = R
menu-edit-cut =
    .label = កាត់
    .accesskey = t
menu-edit-copy =
    .label = ចម្លង
    .accesskey = C
menu-edit-paste =
    .label = បិទភ្ជាប់
    .accesskey = P
menu-edit-delete =
    .label = លុប
    .accesskey = D
menu-edit-select-all =
    .label = ជ្រើស​ទាំងអស់
    .accesskey = A
menu-edit-find-on =
    .label = ស្វែងរក​ក្នុង​ទំព័រ​នេះ...
    .accesskey = F
menu-edit-find-again =
    .label = រក​ម្ដងទៀត
    .accesskey = g
menu-edit-bidi-switch-text-direction =
    .label = ប្ដូរ​​ទិស​អត្ថបទ
    .accesskey = w

## View Menu

menu-view =
    .label = មើល
    .accesskey = V
menu-view-toolbars-menu =
    .label = របារ​ឧបករណ៍
    .accesskey = T
menu-view-customize-toolbar =
    .label = ប្ដូរ​តាម​តម្រូវ​ការ…
    .accesskey = C
menu-view-sidebar =
    .label = របារ​ចំហៀង
    .accesskey = e
menu-view-bookmarks =
    .label = ចំណាំ
menu-view-history-button =
    .label = ប្រវត្តិ
menu-view-synced-tabs-sidebar =
    .label = ផ្ទាំង​ដែល​បាន​ធ្វើ​សមកាលកម្ម
menu-view-full-zoom =
    .label = ពង្រីក
    .accesskey = Z
menu-view-full-zoom-enlarge =
    .label = ​ពង្រីក
    .accesskey = I
menu-view-full-zoom-reduce =
    .label = ​បង្រួម
    .accesskey = O
menu-view-full-zoom-reset =
    .label = កំណត់​ឡើង​វិញ
    .accesskey = R
menu-view-full-zoom-toggle =
    .label = ពង្រីក​តែអត្ថបទ​ប៉ុណ្ណោះ
    .accesskey = T
menu-view-page-style-menu =
    .label = រចនាប័ទ្ម​ទំព័រ
    .accesskey = y
menu-view-page-style-no-style =
    .label = គ្មាន​រចនាប័ទ្ម
    .accesskey = n
menu-view-page-basic-style =
    .label = រចនាប័ទ្ម​ទំព័រ​មូលដ្ឋាន
    .accesskey = b
menu-view-charset =
    .label = ការ​​​អុីន​កូដ​អត្ថបទ
    .accesskey = c

## These should match what Safari and other Apple applications
## use on macOS.

menu-view-enter-full-screen =
    .label = ចូល​អេក្រង់​ពេញ
    .accesskey = F
menu-view-exit-full-screen =
    .label = បិទ​អេក្រង់​ពេញ
    .accesskey = F
menu-view-full-screen =
    .label = អេក្រង់​ពេញ
    .accesskey = F

##

menu-view-show-all-tabs =
    .label = បង្ហាញ​ផ្ទាំង​ទាំងអស់
    .accesskey = A
menu-view-bidi-switch-page-direction =
    .label = ប្ដូរទិស​ទំព័រ
    .accesskey = D

## History Menu

menu-history =
    .label = ប្រវត្តិ
    .accesskey = s
menu-history-show-all-history =
    .label = បង្ហាញ​ប្រវត្តិ​ទាំងអស់
menu-history-clear-recent-history =
    .label = សម្អាត​ប្រវត្តិ​ថ្មីៗ…
menu-history-synced-tabs =
    .label = ផ្ទាំង​ដែល​បាន​ធ្វើ​សមកាលកម្ម
menu-history-restore-last-session =
    .label = ស្ដារ​សម័យ​មុន
menu-history-undo-menu =
    .label = ផ្ទាំង​ដែល​បិទ​ថ្មីៗ
menu-history-undo-window-menu =
    .label = បង្អួច​ដែល​បាន​បិទ​ថ្មីៗ

## Bookmarks Menu

menu-bookmarks-menu =
    .label = ចំណាំ
    .accesskey = B
menu-bookmarks-show-all =
    .label = បង្ហាញ​ចំណាំ​ទាំងអស់
menu-bookmarks-all-tabs =
    .label = ចំណាំ​ផ្ទាំង​ទាំងអស់…
menu-bookmarks-toolbar =
    .label = របារ​ឧបករណ៍​ចំណាំ
menu-bookmarks-other =
    .label = ចំណាំ​ផ្សេង​ៗ​ទៀត
menu-bookmarks-mobile =
    .label = ចំណាំ​ចល័ត

## Tools Menu

menu-tools =
    .label = ឧបករណ៍
    .accesskey = T
menu-tools-downloads =
    .label = ទាញ​យក
    .accesskey = D
menu-tools-addons =
    .label = កម្មវិធី​បន្ថែម
    .accesskey = A
menu-tools-sync-sign-in =
    .label = ចូល​ទៅ { -sync-brand-short-name }…
    .accesskey = Y
menu-tools-sync-now =
    .label = ធ្វើ​សមកាលកម្ម​ឥឡូវ
    .accesskey = S
menu-tools-sync-re-auth =
    .label = តភ្ជាប់​ឡើង​វិញ​ទៅកាន់ { -sync-brand-short-name }…
    .accesskey = R
menu-tools-web-developer =
    .label = អ្នក​អភិវឌ្ឍន៍​បណ្ដាញ
    .accesskey = W
menu-tools-page-source =
    .label = ប្រភព​ទំព័រ
    .accesskey = o
menu-tools-page-info =
    .label = ព័ត៌មាន​ទំព័រ
    .accesskey = I
menu-preferences =
    .label =
        { PLATFORM() ->
            [windows] ជម្រើស
           *[other] ចំណូលចិត្ត
        }
    .accesskey =
        { PLATFORM() ->
            [windows] O
           *[other] n
        }

## Window Menu

menu-window-menu =
    .label = បង្អួច
menu-window-bring-all-to-front =
    .label = នាំទៅ​មុខ​ទាំងអស់

## Help Menu

