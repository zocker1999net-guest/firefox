# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

addons-window =
    .title = מנהל התוספות
search-header =
    .placeholder = חיפוש ב־addons.mozilla.org
    .searchbuttonlabel = חיפוש
search-header-shortcut =
    .key = f
loading-label =
    .value = בטעינה…
list-empty-installed =
    .value = לא הותקנה אף תוספת מסוג זה
list-empty-available-updates =
    .value = לא נמצאו עדכונים
list-empty-recent-updates =
    .value = לא עדכנת תוספות כלשהן לאחרונה
list-empty-find-updates =
    .label = בדיקת עדכונים
list-empty-button =
    .label = מידע נוסף על תוספות
install-addon-from-file =
    .label = התקנת תוספת מקובץ…
    .accesskey = ה
help-button = תמיכה בתוספות
preferences =
    { PLATFORM() ->
        [windows] אפשרויות של { -brand-short-name }
       *[other] העדפות של { -brand-short-name }
    }
tools-menu =
    .tooltiptext = כלים עבור כל התוספות
show-unsigned-extensions-button =
    .label = אין אפשרות לאמת חלק מההרחבות
show-all-extensions-button =
    .label = הצגת כל ההרחבות
debug-addons =
    .label = ניפוי שגיאות של תוספות
    .accesskey = נ
cmd-show-details =
    .label = הצגת מידע נוסף
    .accesskey = ה
cmd-find-updates =
    .label = בדיקת עדכונים
    .accesskey = מ
cmd-preferences =
    .label =
        { PLATFORM() ->
            [windows] אפשרויות
           *[other] העדפות
        }
    .accesskey =
        { PLATFORM() ->
            [windows] א
           *[other] ה
        }
cmd-enable-theme =
    .label = לבש ערכת נושא
    .accesskey = ל
cmd-disable-theme =
    .label = הפסק ללבוש ערכת נושא
    .accesskey = ה
cmd-install-addon =
    .label = התקנה
    .accesskey = ה
cmd-contribute =
    .label = תרומה
    .accesskey = ת
    .tooltiptext = תרומה לפיתוח של תוספת זו
discover-title = מה הן תוספות?
discover-description = תוספות הן יישומים המאפשרים לך להתאים אישית את { -brand-short-name } עם סגנון  או פונקציונליות נוספים. נסה סרגל־צד חוסך זמן, מדווח מזג אוויר, או מראה שונה כדי להפוך את { -brand-short-name }  לשלך.
discover-footer =
    כאשר החיבור לרשת פעיל, חלונית זו תציג
    חלק מהתוספות הטובות והפופולריות ביותר אותן כדאי לנסות.
detail-version =
    .label = גרסה
detail-last-updated =
    .label = עודכן לאחרונה
detail-contributions-description = המפתחים של תוספת זו מבקשים את עזרתך בהמשך הפיתוח שלה על־ידי מתן תרומה צנועה.
detail-contributions-button = תרומה
    .title = מתן תרומה לפיתוח תוספת זו
    .accesskey = ת
detail-update-type =
    .value = עדכונים אוטומטיים
detail-update-default =
    .label = בררת מחדל
    .tooltiptext = התקנת עדכונים באופן אוטומטי רק אם זו בררת המחדל
detail-update-automatic =
    .label = פעיל
    .tooltiptext = התקן עדכונים אוטומטית
detail-update-manual =
    .label = כבוי
    .tooltiptext = אל תתקין עדכונים אוטומטית
# Used as a description for the option to allow or block an add-on in private windows.
detail-private-browsing-label = הפעלה בחלונות פרטיים
detail-private-browsing-description2 = כאשר אפשרות זו מופעלת, להרחבה תהיה גישה לפעילויות המקוונות שלך בזמן גלישה פרטית. <label data-l10n-name="detail-private-browsing-learn-more">מידע נוסף</label>
# Some add-ons may elect to not run in private windows by setting incognito: not_allowed in the manifest.  This
# cannot be overridden by the user.
detail-private-disallowed-label = לא מופעלת בחלונות פרטיים
detail-private-disallowed-description = הרחבה זו לא פועלת בזמן גלישה פרטית. <label data-l10n-name="detail-private-browsing-learn-more">מידע נוסף</label>
# Some special add-ons are privileged, run in private windows automatically, and this permission can't be revoked
detail-private-required-label = דורשת גישה לחלונות פרטיים
detail-private-required-description = להרחבה זו יש גישה לפעילויות  המקוונות שלך בזמן גלישה פרטית. <label data-l10n-name="detail-private-browsing-learn-more">מידע נוסף</label>
detail-private-browsing-on =
    .label = לאפשר
    .tooltiptext = הפעלה בגלישה פרטית
detail-private-browsing-off =
    .label = לא לאפשר
    .tooltiptext = נטרול בגלישה פרטית
detail-home =
    .label = דף הבית
detail-home-value =
    .value = { detail-home.label }
detail-repository =
    .label = פרופיל התוספת
detail-repository-value =
    .value = { detail-repository.label }
detail-check-for-updates =
    .label = בדיקת עדכונים
    .accesskey = ב
    .tooltiptext = בדיקת עדכונים לתוספת זו
detail-show-preferences =
    .label =
        { PLATFORM() ->
            [windows] אפשרויות
           *[other] העדפות
        }
    .accesskey =
        { PLATFORM() ->
            [windows] א
           *[other] ה
        }
    .tooltiptext =
        { PLATFORM() ->
            [windows] שינוי האפשרויות של תוספת זו
           *[other] שינוי העדפות של תוספת זו
        }
detail-rating =
    .value = דירוג
addon-restart-now =
    .label = הפעלה מחדש כעת
disabled-unsigned-heading =
    .value = חלק מהתוספות נוטרלו
disabled-unsigned-description = התוספות הבאות לא אומתו לשימוש ב־{ -brand-short-name }. באפשרותך <label data-l10n-name="find-addons">לחפש חלופות</label> או לפנות למפתחים בדרישה לאמת אותן.
disabled-unsigned-learn-more = מידע נוסף על המאמצים שלנו לשמור על המשתמשים שלנו בטוחים ברשת.
disabled-unsigned-devinfo = מפתחים המעוניינים לאמת את התוספות שלהם מתבקשים לעיין <label data-l10n-name="learn-more">במדריך</label> שלנו.
plugin-deprecation-description = חסר כאן משהו? חלק מהתוספים החיצוניים אינם נתמכים עוד ב־{ -brand-short-name }. <label data-l10n-name="learn-more">למידע נוסף.</label>
legacy-warning-show-legacy = הצגת הרחבות דור קודם
legacy-extensions =
    .value = הרחבות מדור קודם
legacy-extensions-description = הרחבות אלו לא עומדות בתקנים הנוכחיים של { -brand-short-name } ולכן כובו. <label data-l10n-name="legacy-learn-more">מידע נוסף על השינויים בתוספות</label>
private-browsing-description2 =
    ‏{ -brand-short-name } משנה את האופן שבו הרחבות פועלות בגלישה פרטית. כל הרחבה חדשה שתתווסף אל { -brand-short-name } לא תרוץ כברירת מחדל בחלונות פרטיים. כל עוד אפשרות זו לא תופעל בהגדרות, ההרחבה לא תפעל בזמן גלישה פרטית, ולא תהיה לה גישה לפעילויות המקוונות שלך שם. עשינו את השינוי הזה כדי לשמור על הגלישה הפרטית שלך פרטית.
    <label data-l10n-name="private-browsing-learn-more">מידע נוסף על ניהול הגדרות הרחבות.</label>
extensions-view-discopane =
    .name = המלצות
    .tooltiptext = { extensions-view-discopane.name }
extensions-view-recent-updates =
    .name = עדכונים אחרונים
    .tooltiptext = { extensions-view-recent-updates.name }
extensions-view-available-updates =
    .name = עדכונים זמינים
    .tooltiptext = { extensions-view-available-updates.name }

## These are global warnings

extensions-warning-safe-mode-label =
    .value = כל התוספות נוטרלו במצב בטוח.
extensions-warning-safe-mode-container =
    .tooltiptext = { extensions-warning-safe-mode-label.value }
extensions-warning-check-compatibility-label =
    .value = בדיקת תאימות תוספות מנוטלת. ייתכן וברשותך הרחבות לא תואמות.
extensions-warning-check-compatibility-container =
    .tooltiptext = { extensions-warning-check-compatibility-label.value }
extensions-warning-check-compatibility-enable =
    .label = הפעלה
    .tooltiptext = הפעלת בדיקת תאימות תוספות
extensions-warning-update-security-label =
    .value = בדיקת האבטחה של התוספות כרגע מנוטרלת. עדכונים לתוספות עלולים לסכן אותך.
extensions-warning-update-security-container =
    .tooltiptext = { extensions-warning-update-security-label.value }
extensions-warning-update-security-enable =
    .label = הפעלה
    .tooltiptext = הפעלת בדיקות אבטחה לעדכוני תוספות

## Strings connected to add-on updates

extensions-updates-check-for-updates =
    .label = בדיקת עדכונים
    .accesskey = ב
extensions-updates-view-updates =
    .label = הצגת עדכונים אחרונים
    .accesskey = ה

# This menu item is a checkbox that toggles the default global behavior for
# add-on update checking.

extensions-updates-update-addons-automatically =
    .label = עדכון תוספות אוטומטי
    .accesskey = ע

## Specific add-ons can have custom update checking behaviors ("Manually",
## "Automatically", "Use default global behavior"). These menu items reset the
## update checking behavior for all add-ons to the default global behavior
## (which itself is either "Automatically" or "Manually", controlled by the
## extensions-updates-update-addons-automatically.label menu item).

extensions-updates-reset-updates-to-automatic =
    .label = איפוס כל התוספות לעדכון אוטומטי
    .accesskey = א
extensions-updates-reset-updates-to-manual =
    .label = איפוס כל התוספות לעדכון ידני
    .accesskey = א

## Status messages displayed when updating add-ons

extensions-updates-updating =
    .value = מעדכן תוספות
extensions-updates-installed =
    .value = התוספות שלך עודכנו.
extensions-updates-downloaded =
    .value = עדכוני התוספות שלך הורדו.
extensions-updates-restart =
    .label = הפעלה מחדש כעת להשלמת ההתקנה
extensions-updates-none-found =
    .value = לא נמצאו עדכונים
extensions-updates-manual-updates-found =
    .label = הצגת עדכונים זמינים
extensions-updates-update-selected =
    .label = התקנת עדכונים
    .tooltiptext = התקנת העדכונים הזמינים ברשימה זו

## Extension shortcut management

manage-extensions-shortcuts =
    .label = ניהול קיצורי דרך להרחבות
    .accesskey = נ
shortcuts-no-addons = אין לך הרחבות מופעלות.
shortcuts-no-commands = להרחבות הבאות אין קיצורי דרך:
shortcuts-input =
    .placeholder = נא להקליד קיצור דרך
shortcuts-browserAction = הפעלת הרחבה
shortcuts-pageAction = הפעלת פעולת דף
shortcuts-sidebarAction = הצגה/הסתרה של סרגל הצד
shortcuts-modifier-mac = יש לכלול Ctrl, ‏Alt או ⌘
shortcuts-modifier-other = יש לכלול Ctrl או Alt
shortcuts-invalid = שילוב לא חוקי
shortcuts-letter = נא להקליד אות
shortcuts-system = לא ניתן לדרוס קיצור דרך של { -brand-short-name }
# String displayed in warning label when there is a duplicate shortcut
shortcuts-duplicate = קיצור דרך כפול
# String displayed when a keyboard shortcut is already assigned to more than one add-on
# Variables:
#   $shortcut (string) - Shortcut string for the add-on
shortcuts-duplicate-warning-message = { $shortcut } משמש כקיצור דרך ביותר ממקרה אחד. קיצורי דרך כפולים עשויים לגרום להתנהגות בלתי צפויה.
# String displayed when a keyboard shortcut is already used by another add-on
# Variables:
#   $addon (string) - Name of the add-on
shortcuts-exists = כבר בשימוש על־ידי { $addon }
shortcuts-card-expand-button =
    { $numberToShow ->
        [one] הצגת אחד נוסף
       *[other] הצגת { $numberToShow } נוספים
    }
shortcuts-card-collapse-button = הצגת פחות
go-back-button =
    .tooltiptext = חזרה אחורה

## Recommended add-ons page

# Explanatory introduction to the list of recommended add-ons. The action word
# ("recommends") in the final sentence is a link to external documentation.
discopane-intro = הרחבות הן כמו יישומים לדפדפן שלך, ומאפשרות לך להגן על ססמאות, להוריד סרטונים, למצוא מבצעים, לחסום פרסומות מציקות, לשנות את תצוגת הדפדפן שלך ועוד. היישומים הקטנים האלו לרוב מפותחים על־ידי גורמי צד־שלישי. להלן מבחר הרחבות ש־{ -brand-product-name } <a data-l10n-name="learn-more-trigger">ממליצה</a> עליהן בגלל האבטחה, הביצועים והפונקציונליות יוצאת הדופן שלהן.
# Notice to make user aware that the recommendations are personalized.
discopane-notice-recommendations = חלק מהמלצות אלה מותאמות אישית. הן מבוססות על הרחבות אחרות שהתקנת, העדפות פרופיל וסטטיסטיקת שימוש.
discopane-notice-learn-more = מידע נוסף
privacy-policy = מדיניות פרטיות
# Refers to the author of an add-on, shown below the name of the add-on.
# Variables:
#   $author (string) - The name of the add-on developer.
created-by-author = מאת <a data-l10n-name="author">{ $author }</a>
# Shows the number of daily users of the add-on.
# Variables:
#   $dailyUsers (number) - The number of daily users.
user-count = משתמשים: { $dailyUsers }
install-extension-button = הוספה אל { -brand-product-name }
install-theme-button = התקנת ערכת נושא
# The label of the button that appears after installing an add-on. Upon click,
# the detailed add-on view is opened, from where the add-on can be managed.
manage-addon-button = ניהול
find-more-addons = חיפוש תוספות נוספות

## Add-on actions

report-addon-button = דיווח
remove-addon-button = הסרה
disable-addon-button = השבתה
enable-addon-button = הפעלה
expand-addon-button = אפשרויות נוספות
preferences-addon-button =
    { PLATFORM() ->
        [windows] אפשרויות
       *[other] העדפות
    }
details-addon-button = פרטים
release-notes-addon-button = הערות שחרור
permissions-addon-button = הרשאות
addons-enabled-heading = מופעלות
addons-disabled-heading = מושבתות
ask-to-activate-button = בקשת אישור להפעלה
always-activate-button = הפעלה תמיד
never-activate-button = לא להפעיל לעולם
addon-detail-author-label = מפתח
addon-detail-version-label = גרסה
addon-detail-last-updated-label = עדכון אחרון
addon-detail-homepage-label = דף הבית
addon-detail-rating-label = דירוג
# The average rating that the add-on has received.
# Variables:
#   $rating (number) - A number between 0 and 5. The translation should show at most one digit after the comma.
five-star-rating =
    .title = דירוג { NUMBER($rating, maximumFractionDigits: 1) } מתוך 5
# This string is used to show that an add-on is disabled.
# Variables:
#   $name (string) - The name of the add-on
addon-name-disabled = ‏{ $name } (מושבת)
# The number of reviews that an add-on has received on AMO.
# Variables:
#   $numberOfReviews (number) - The number of reviews received
addon-detail-reviews-link =
    { $numberOfReviews ->
        [one] סקירה אחת
       *[other] { $numberOfReviews } סקירות
    }

## Pending uninstall message bar

# Variables:
#   $addon (string) - Name of the add-on
pending-uninstall-description = התוספת <span data-l10n-name="addon-name">{ $addon }</span> הוסרה.
pending-uninstall-undo-button = ביטול
addon-detail-updates-label = עדכונים אוטומטיים
addon-detail-updates-radio-default = ברירת מחדל
addon-detail-updates-radio-on = פעיל
addon-detail-updates-radio-off = כבוי
addon-detail-update-check-label = בדיקת עדכונים
install-update-button = עדכון
# This is the tooltip text for the private browsing badge in about:addons. The
# badge is the private browsing icon included next to the extension's name.
addon-badge-private-browsing-allowed =
    .title = מופעלת בחלונות פרטיים
addon-detail-private-browsing-help = אם אפשרות זו מופעלת, להרחבה תהיה גישה לפעילויות המקוונות שלך בזמן גלישה פרטית. <a data-l10n-name="learn-more">מידע נוסף</a>
addon-detail-private-browsing-allow = לאפשר
addon-detail-private-browsing-disallow = לא לאפשר
# This is the tooltip text for the recommended badge for an extension in about:addons. The
# badge is a small icon displayed next to an extension when it is recommended on AMO.
addon-badge-recommended =
    .title = מומלצת
    .alt = מומלצת
available-updates-heading = עדכונים זמינים
recent-updates-heading = עדכונים אחרונים
release-notes-loading = בטעינה…
release-notes-error = מצטערים, אירעה שגיאה במהלך טעינת הערות השחרור.
addon-permissions-empty = לתוספת זו לא נדרשות הרשאות
recommended-extensions-heading = הרחבות מומלצות
recommended-themes-heading = ערכות נושא מומלצות
# A recommendation for the Firefox Color theme shown at the bottom of the theme
# list view. The "Firefox Color" name itself should not be translated.
recommended-theme-1 = גל של יצירתיות שוטף אותך? <a data-l10n-name="link">ניתן ליצור ערכת נושא משלך בעזרת Firefox Color.</a>
