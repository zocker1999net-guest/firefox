# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = ਕਰੈਸ਼ ਰਿਪੋਰਟਾਂ
delete-button-label = ਸਾਰੇ ਸਾਫ਼ ਕਰੋ
delete-confirm-title = ਕੀ ਤੁਸੀਂ ਤਸਦੀਕ ਕਰਦੇ ਹੋ?
id-heading = ਰਿਪੋਰਟ ID
date-crashed-heading = ਕਰੈਸ਼ ਦੀ ਮਿਤੀ
submit-crash-button-label = ਭੇਜੋ
# This text is used to replace the label of the crash submit button
# if the crash submission fails.
submit-crash-button-failure-label = ਅਸਫ਼ਲ ਹੈ
no-reports-label = ਕੋਈ ਕਰੈਸ਼ ਰਿਪੋਰਟ ਨਹੀਂ ਦਿੱਤੀ ਗਈ
no-config-label = ਇਹ ਐਪਲੀਕੇਸ਼ਨ ਕਰੈਸ਼ ਰਿਪੋਰਟਾਂ ਵੇਖਾਉਣ ਲਈ ਸੰਰਚਿਤ ਨਹੀਂ ਕੀਤੀ ਗਈ ਹੈ। <code>breakpad.reportURL</code> ਸੈੱਟ ਹੋਣਾ ਚਾਹੀਦਾ ਹੈ।
