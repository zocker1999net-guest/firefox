<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- addon actions -->
<!ENTITY cmd.enableAddon.label                "ਚਾਲੂ">
<!ENTITY cmd.enableAddon.accesskey            "E">
<!ENTITY cmd.disableAddon.label               "ਬੰਦ">
<!ENTITY cmd.disableAddon.accesskey           "D">

<!ENTITY cmd.askToActivate.label              "ਸਰਗਰਮ ਕਰਨ ਲਈ ਪੁੱਛੋ">
<!ENTITY cmd.askToActivate.tooltip            "ਇਹ ਐਡ-ਆਨ ਨੂੰ ਹਰ ਵਾਰ ਵਰਤਣ ਸਮੇਂ ਪੁੱਛੋ">
<!ENTITY cmd.alwaysActivate.label             "ਹਮੇਸ਼ਾ ਸਰਗਰਮ ਕਰੋ">
<!ENTITY cmd.alwaysActivate.tooltip           "ਇਹ ਐਡ-ਆਨ ਹਮੇਸ਼ਾ ਵਰਤੋਂ">
<!ENTITY cmd.neverActivate.label              "ਕਦੇ ਸਰਗਰਮ ਨਾ ਕਰੋ">
<!ENTITY cmd.neverActivate.tooltip            "ਇਹ ਐਡ-ਆਨ ਕਦੇ ਨਾ ਵਰਤੋਂ">
<!ENTITY cmd.stateMenu.tooltip                "ਜਦੋਂ ਇਹ ਐਡ-ਆਨ ਚੱਲੇ ਤਾਂ ਬਦਲੋ">

<!ENTITY cmd.uninstallAddon.label             "ਹਟਾਓ">
<!ENTITY cmd.uninstallAddon.accesskey         "R">
<!ENTITY cmd.showPreferencesWin.label         "ਚੋਣਾਂ">
<!ENTITY cmd.showPreferencesWin.tooltip       "ਇਸ ਐਡ-ਆਨ ਦੀਆਂ ਚੋਣਾਂ ਨੂੰ ਬਦਲੋ">
<!ENTITY cmd.showPreferencesUnix.label        "ਮੇਰੀ ਪਸੰਦ">
<!ENTITY cmd.showPreferencesUnix.tooltip      "ਇਸ ਐਡ-ਆਨ ਦੀ ਪਸੰਦ ਬਦਲੋ">

<!ENTITY cmd.showReleaseNotes.label           "ਰੀਲਿਜ਼ ਨੋਟਿਸ ਵੇਖੋ">
<!ENTITY cmd.showReleaseNotes.tooltip         "ਇਸ ਅੱਪਡੇਟ ਲਈ ਰੀਲਿਜ਼ ਨੋਟਿਸ ਵੇਖੋ">
<!ENTITY cmd.hideReleaseNotes.label           "ਰੀਲਿਜ਼ ਨੋਟਿਸ ਓਹਲੇ">
<!ENTITY cmd.hideReleaseNotes.tooltip         "ਇਹ ਅੱਪਡੇਟ ਲਈ ਰੀਲਿਜ਼ ਨੋਟਿਸ ਓਹਲੇ ਕਰੋ">
<!ENTITY cmd.findReplacement.label            "ਬਦਲ ਲੱਭੋ">

<!-- download/install progress -->
<!ENTITY progress.cancel.tooltip              "ਰੱਦ ਕਰੋ">

<!ENTITY addon.details.label                  "ਹੋਰ">
<!ENTITY addon.details.tooltip                "ਇਸ ਐਡ-ਆਨ ਬਾਰੇ ਹੋਰ ਵੇਰਵਾ ਵੇਖੋ">
<!ENTITY addon.unknownDate                    "ਅਣਜਾਣ">
<!-- LOCALIZATION NOTE (addon.legacy.label): This appears in a badge next
     to the add-on name for extensions that are not webextensions, which
     will stop working in Firefox 57. -->
<!ENTITY addon.legacy.label                   "ਪੁਰਾਣਾ">
<!ENTITY addon.privateBrowsing.label          "ਪ੍ਰਾਈਵੇਟ ਵਿੰਡੋਆਂ ‘ਚ ਆਗਿਆ ਦਿੱਤੀ">
<!-- LOCALIZATION NOTE (addon.disabled.postfix): This is used in a normal list
     to signify that an add-on is disabled, in the form
     "<Addon name> <1.0> (disabled)" -->
<!ENTITY addon.disabled.postfix               "(ਬੰਦ ਹੈ)">
<!-- LOCALIZATION NOTE (addon.update.postfix): This is used in the available
     updates list to signify that an item is an update, in the form
     "<Addon name> <1.1> Update". It is fine to use constructs like brackets if
     necessary -->
<!ENTITY addon.update.postfix                 "ਅੱਪਡੇਟ">
<!ENTITY addon.undoAction.label               "ਵਾਪਸ">
<!ENTITY addon.undoAction.tooltip             "ਇਹ ਕਾਰਵਾਈ ਵਾਪਸ ਲਵੋ">
<!ENTITY addon.undoRemove.label               "ਵਾਪਸ">
<!ENTITY addon.undoRemove.tooltip             "ਇਹ ਐਡ-ਆਨ ਇੰਸਟਾਲ ਹੀ ਰੱਖੋ">

<!ENTITY addon.install.label                  "ਇੰਸਟਾਲ ਕਰੋ">
<!ENTITY addon.install.tooltip                "ਇਹ ਐਡ-ਆਨ ਇੰਸਟਾਲ ਕਰੋ">
<!ENTITY addon.updateNow.label                "ਹੁਣੇ ਅੱਪਡੇਟ ਕਰੋ">
<!ENTITY addon.updateNow.tooltip              "ਇਹ ਐਡ-ਆਨ ਲਈ ਅੱਪਡੇਟ ਇੰਸਟਾਲ ਕਰੋ">
<!ENTITY addon.includeUpdate.label            "ਅੱਪਡੇਟ 'ਚ ਸ਼ਾਮਲ ਕਰੋ">
<!ENTITY addon.updateAvailable.label          "ਅੱਪਡੇਟ ਉਪਲੱਬਧ ਹੈ">
<!ENTITY addon.checkingForUpdates.label       "…ਅੱਪਡੇਟ ਲਈ ਚੈੱਕ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ">
<!ENTITY addon.releaseNotes.label             "ਰੀਲਿਜ਼ ਨੋਟਿਸ:">
<!ENTITY addon.loadingReleaseNotes.label      "…ਲੋਡ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ">
<!ENTITY addon.errorLoadingReleaseNotes.label "ਅਫਸੋਸ, ਪਰ ਰੀਲਿਜ਼ ਨੋਟਿਸ ਡਾਊਨਲੋਡ ਕਰਨ ਦੌਰਾਨ ਸਮੱਸਿਆ ਆਈ ਹੈ।">

<!ENTITY addon.createdBy.label                "ਵਲੋਂ ">

<!ENTITY settings.path.button.label           "…ਝਲਕ">
<!ENTITY setting.learnmore "…ਹੋਰ ਜਾਣੋ">
