# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## These messages are used as headings in the recommendation doorhanger

cfr-doorhanger-extension-heading = ਸਿਫ਼ਾਰਸ਼ ਕੀਤੀ ਇਕਸਟੈਨਸ਼ਨ
cfr-doorhanger-feature-heading = ਸਿਫਾਰਸ਼ ਕੀਤੇ ਫੀਚਰ
cfr-doorhanger-pintab-heading = ਇਹ ਕਰਨ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰੋ: ਪਿੰਨ ਟੈਬ



cfr-doorhanger-extension-sumo-link =
    .tooltiptext = ਮੈਂ ਇਹ ਕਿਓ ਵੇਖ ਰਿਹਾ/ਰਹੀ ਹਾਂ
cfr-doorhanger-extension-cancel-button = ਹੁਣ ਨਹੀਂ
    .accesskey = N
cfr-doorhanger-extension-ok-button = ਹੁਣੇ ਜੋੜੋ
    .accesskey = A
cfr-doorhanger-pintab-ok-button = ਇਸ ਟੈਬ ਨੂੰ ਪਿੰਨ ਕਰੋ
    .accesskey = P
cfr-doorhanger-extension-manage-settings-button = ਸਿਫਾਰਸ਼ੀ ਸੈਟਿੰਗਾਂ ਦਾ ਬੰਦੋਬਸਤ ਕਰੋ
    .accesskey = M
cfr-doorhanger-extension-never-show-recommendation = ਇਹ ਸਿਫਾਰਸ਼ ਮੈਨੂੰ ਨਾ ਦੇਖਾਓ
    .accesskey = S
cfr-doorhanger-extension-learn-more-link = ਹੋਰ ਸਿੱਖੋ
# This string is used on a new line below the add-on name
# Variables:
#   $name (String) - Add-on author name
cfr-doorhanger-extension-author = { $name } ਰਾਹੀਂ
# This is a notification displayed in the address bar.
# When clicked it opens a panel with a message for the user.
cfr-doorhanger-extension-notification = ਸਿਫ਼ਾਰਸ਼ੀ

## Add-on statistics
## These strings are used to display the total number of
## users and rating for an add-on. They are shown next to each other.

# Variables:
#   $total (Number) - The rating of the add-on from 1 to 5
cfr-doorhanger-extension-rating =
    .tooltiptext =
        { $total ->
            [one] { $total } ਤਾਰਾ
           *[other] { $total } ਤਾਰੇ
        }
# Variables:
#   $total (Number) - The total number of users using the add-on
cfr-doorhanger-extension-total-users =
    { $total ->
        [one] { $total } ਵਰਤੋਂਕਾਰ
       *[other] { $total } ਵਰਤੋਂਕਾਰ
    }
cfr-doorhanger-pintab-description = ਆਪਣੀ ਸਭ ਤੋਂ ਜ਼ਿਆਦਾ ਵਰਤੋਂ ਵਾਲੀਆਂ ਸਾਈਟਾਂ ਤੱਕ ਆਸਾਨ ਪਹੁੰਚ ਪ੍ਰਾਪਤ ਕਰੋ ਸਾਈਟਾਂ ਨੂੰ ਇੱਕ ਟੈਬ ਵਿੱਚ ਰੱਖੋ (ਭਾਵੇਂ ਤੁਸੀਂ ਮੁੜ ਚਾਲੂ ਕਰੋ)।

## These messages are steps on how to use the feature and are shown together.

cfr-doorhanger-pintab-step1 = ਜਿਸ ਟੈਬ ਨੂੰ ਤੁਸੀਂ ਟੰਗਣਾ ਚਾਹੁੰਦੇ ਹੋ ਉਸ ਉੱਤੇ <b>ਰਾਈਟ-ਕਲਿੱਕ</ b>।
cfr-doorhanger-pintab-step2 = ਮੇਨੂ ਤੋਂ <b>ਟੈਬ ਨੂੰ ਟੰਗੋ</b> ਨੂੰ ਚੁਣੋ।
cfr-doorhanger-pintab-step3 = ਜੇ ਸਾਈਟ ਨੂੰ ਅੱਪਡੇਟ ਕੀਤਾ ਗਿਆ ਤਾਂ ਤੁਸੀਂ ਆਪਣੀ ਟੰਗ ਕੀਤੀ ਟੈਬ ਵਿੱਚ ਨੀਲੀ ਬਿੰਦੀ ਵੇਖੋਗੇ।
cfr-doorhanger-pintab-animation-pause = ਵਿਰਾਮ
cfr-doorhanger-pintab-animation-resume = ਮੁੜ-ਪ੍ਰਾਪਤ

## Firefox Accounts Message

cfr-doorhanger-bookmark-fxa-header = ਆਪਣੇ ਬੁੱਕਮਾਰਕ ਹਰ ਥਾਂ ਉੱਤੇ ਸਿੰਕ ਕਰੋ।
cfr-doorhanger-bookmark-fxa-link-text = …ਬੁੱਕਮਾਰਕ ਹੁਣੇ ਸਿੰਕ ਕਰੋ
cfr-doorhanger-bookmark-fxa-close-btn-tooltip =
    .aria-label = ਬੰਦ ਕਰੋ ਬਟਨ
    .title = ਬੰਦ ਕਰੋ

## Protections panel

cfr-protections-panel-header = ਬਿਨਾਂ ਪਿੱਛਾ ਕਰਵਾਏ ਬਰਾਊਜ਼ ਕਰੋ
cfr-protections-panel-link-text = ਹੋਰ ਜਾਣੋ

## What's New toolbar button and panel

cfr-whatsnew-button =
    .label = ਨਵਾਂ ਕੀ ਹੈ
    .tooltiptext = ਨਵਾਂ ਕੀ ਹੈ
cfr-whatsnew-panel-header = ਨਵਾਂ ਕੀ ਹੈ

## Bookmark Sync

cfr-doorhanger-sync-bookmarks-header = ਇਸ ਬੁੱਕਮਾਰਕ ਨੂੰ ਆਪਣੇ ਫ਼ੋਨ ‘ਤੇ ਲਵੋ
cfr-doorhanger-sync-bookmarks-body = ਆਪਣੇ ਬੁੱਕਮਾਰਕ, ਪਾਸਵਰਡ, ਅਤੀਤ ਅਤੇ ਹੋਰ ਨੂੰ ਹਰ ਥਾਂ ਆਪਣੇ ਨਾਲ { -brand-product-name } ‘ਚ ਸਾਈਨ ਇਨ ਕਰਕੇ ਲੈ ਜਾਓ।
cfr-doorhanger-sync-bookmarks-ok-button = { -sync-brand-short-name } ਚਲਾੂ ਕਰੋ
    .accesskey = T

## Login Sync

cfr-doorhanger-sync-logins-header = ਮੁੜ ਕੇ ਪਾਸਵਰਡ ਕਦੇ ਵੀ ਨਾ ਗੁਆਓ
cfr-doorhanger-sync-logins-body = ਆਪਣੇ ਸਾਰੇ ਡਿਵਾਈਸਾਂ ‘ਤੇ ਆਪਣੇ ਪਾਸਵਰਡ ਸੁਰੱਖਿਅਤ ਢੰਗ ਨਾਲ ਸੰਭਾਲੋ ਅਤੇ ਸਿੰਕ ਕਰੋ।
cfr-doorhanger-sync-logins-ok-button = { -sync-brand-short-name } ਚਾਲੂ ਕਰੋ
    .accesskey = T

## Send Tab

cfr-doorhanger-send-tab-header = ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਇਹ ਪੜ੍ਹੋ

## Firefox Send

cfr-doorhanger-firefox-send-header = ਇਹ PDF ਨੂੰ ਸੁਰੱਖਿਅਤ ਢੰਗ ਨਾਲ ਸਾਂਝਾ ਕਰੋ
cfr-doorhanger-firefox-send-ok-button = { -send-brand-name } ਵਰਤ ਕੇ ਵੇਖੋ
    .accesskey = T
