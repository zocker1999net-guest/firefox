# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = Vali keeled, mida kasutatakse menüüde, sõnumite ja { -brand-short-name }ilt tulevate teavituste kuvamiseks.
manage-messenger-languages-button =
    .label = Määra alternatiivsed keeled…
    .accesskey = M
confirm-messenger-language-change-description = Muudatuste rakendamiseks taaskäivita { -brand-short-name }
confirm-messenger-language-change-button = Rakenda ja taaskäivita
update-pref-write-failure-title = Viga kirjutamisel
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = Sätete salvestamine polnud võimalik. Järgmisse faili polnud võimalik kirjutada: { $path }
update-setting-write-failure-title = Uuendamise sätete salvestamisel esines viga
# Variables:
#   $path (String) - Path to the configuration file
# The newlines between the main text and the line containing the path is
# intentional so the path is easier to identify.
update-setting-write-failure-message =
    { -brand-short-name }il esines viga ja muudatust ei salvestatud. Antud sätte muutmiseks on vajalikud õigused alloleva faili muutmiseks. Probleem võib laheneda, kui sina või sinu süsteemiadministraator annab Users grupile täielikud muutmise õigused sellele failile.
    
    Järgmist faili polnud võimalik muuta: { $path }
update-in-progress-title = Uuendamine
update-in-progress-message = Kas soovid, et { -brand-short-name } jätkaks uuendamisega?
update-in-progress-ok-button = &Loobu
# Continue is the cancel button so pressing escape or using a platform standard
# method of closing the UI will not discard the update.
update-in-progress-cancel-button = &Jätka
