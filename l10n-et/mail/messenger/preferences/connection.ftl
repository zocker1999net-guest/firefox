# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Teenusepakkuja
    .accesskey = T
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (vaikimisi)
    .tooltiptext = Kasuta üle HTTPSi töötava DNSi puhul vaikeaadressi
connection-dns-over-https-url-custom =
    .label = kohandatud
    .accesskey = o
    .tooltiptext = Sisesta üle HTTPSi töötava DNSi jaoks oma eelistatud URL
connection-dns-over-https-custom-label = Kohandatud
