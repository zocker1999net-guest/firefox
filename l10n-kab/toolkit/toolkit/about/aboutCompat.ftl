# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

label-disable = Ssens
label-enable = Rmed
label-more-information = Ugar n talɣut: Abug { $bug }
text-disabled-in-about-config = Tamahilt-a tensa deg about:config
text-title = about:compat
