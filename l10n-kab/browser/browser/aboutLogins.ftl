# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-logins-page-title = Inekcam & wawalen uffiren

# "Google Play" and "App Store" are both branding and should not be translated

login-filter =
    .placeholder = Nadi inekcam
create-login-button = Rnu anekcum amaynut
fxaccounts-sign-in-button = Qqen ɣer { -sync-brand-short-name }
fxaccounts-avatar-button =
    .title = Sefrek amiḍan

## The ⋯ menu that is in the top corner of the page

menu =
    .title = Ldi umuɣ
# This menuitem is only visible on Windows
menu-menuitem-import = Kter awalen uffiren...
menu-menuitem-preferences =
    { PLATFORM() ->
        [windows] iɣewwaṛen
       *[other] Ismenyifen
    }
menu-menuitem-feedback = Azen tikti
menu-menuitem-faq = Isteqsiyen i d-yettuɣalen s waṭas.

## Login List

login-list-sort-label-text = Smizzwer s:
login-list-name-option = Isem (A-Z)
login-list-last-changed-option = Asnifel aneggaru
login-list-last-used-option = Aseqdec anneggaru
login-list-intro-title = Ulac inekcam yettwafen
login-list-item-title-new-login = Anekcum amaynut
login-list-item-subtitle-missing-username = (ulas isem n useqdac)

## Introduction screen


## Login

login-item-new-login-title = Rnu anekcum amaynut
login-item-edit-button = Ẓreg
login-item-delete-button = Kkes
login-item-origin-label = Tansa n usmel web
login-item-origin =
    .placeholder = https://www.example.com
login-item-open-site-button = Senker
login-item-username-label = Isem n useqdac
login-item-username =
    .placeholder = isem@example.com
login-item-copy-username-button-text = Nɣel
login-item-copied-username-button-text = Inɣel!
login-item-password-label = Awal uffir
login-item-password-reveal-checkbox-show =
    .title = Sken awal uffir
login-item-password-reveal-checkbox-hide =
    .title = Ffer awal uffir
login-item-copy-password-button-text = Nɣel
login-item-copied-password-button-text = Inɣel!
login-item-save-changes-button = Sekles asnifel
login-item-save-new-button = Sekles
login-item-cancel-button = Sefsex

## Master Password notification

master-password-reload-button =
    .label = Kcem
    .accesskey = K

## Dialogs

confirmation-dialog-cancel-button = Sefsex
confirmation-dialog-dismiss-button =
    .title = Sefsex
confirm-delete-dialog-title = Kkes anekcam-agi?
confirm-delete-dialog-message = Ulac tuɣalin ɣer deffir.
confirm-delete-dialog-confirm-button = Kkes
confirm-discard-changes-dialog-title = Sefsex isenfal-agi?
confirm-discard-changes-dialog-confirm-button = Kkes

## Breach Alert notification

breach-alert-dismiss =
    .title = Mdel ulɣu-agi
