# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### UI strings for the simplified onboarding modal / about:welcome
### Various strings use a non-breaking space to avoid a single dangling /
### widowed word, so test on various window sizes if you also want this.


## These button action text can be split onto multiple lines, so use explicit
## newlines in translations to control where the line break appears (e.g., to
## avoid breaking quoted text).

onboarding-button-label-try-now = Próbeo agora
onboarding-button-label-get-started = Comezar

## Welcome modal dialog strings

onboarding-welcome-header = Benvida ao { -brand-short-name }
onboarding-start-browsing-button-label = Iniciar a navegación
onboarding-cards-dismiss =
    .title = Rexeitar
    .aria-label = Rexeitar

## Firefox Sync modal dialog strings.

onboarding-sync-welcome-header = Leve o { -brand-product-name } consigo
onboarding-sync-welcome-content = Acceda aos seus marcadores, historial, contrasinais e outras configuracións en todos os seus dispositivos.
onboarding-sync-welcome-learn-more-link = Obteña máis información sobre as contas Firefox
onboarding-sync-form-invalid-input = Requírese un correo válido
onboarding-sync-legal-notice = Ao continuar, acepta os <a data-l10n-name="terms">Termos do servizo</a> e a <a data-l10n-name="privacy">Política de privacidade</a>.
onboarding-sync-form-input =
    .placeholder = Correo electrónico
onboarding-sync-form-continue-button = Continuar
onboarding-sync-form-skip-login-button = Ignorar este paso

## This is part of the line "Enter your email to continue to Firefox Sync"

onboarding-sync-form-header = Escriba o seu correo
onboarding-sync-form-sub-header = para continuar a { -sync-brand-name }.

## These are individual benefit messages shown with an image, title and
## description.


## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Navegación privada
onboarding-private-browsing-text = Navegue vostede só. A navegación privada con bloqueo de contido bloquea os elementos que o seguen pola web.
onboarding-screenshots-title = Capturas de pantalla
onboarding-screenshots-text = Faga, garde e comparta as capturas de pantalla sen saír do { -brand-short-name }. Capture unha zona ou toda a páxina mentres navega. Despois gárdea na web para acceder a ela e compartila facilmente.
onboarding-addons-title = Complementos
onboarding-addons-text = Engada aínda máis características que fagan que { -brand-short-name } faga máis por vostede. Compare prezos, comprobe o tempo ou exprese a súa personalidade cun tema personalizado.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Navegue más rápido, de forma más intelixente ou máis segura con extensións como Ghostery, que lle permite bloquear os anuncios molestos.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Sincronizar
onboarding-fxa-text = Rexístrese para crear unha { -fxaccount-brand-name } e sincronice os marcadores, os contrasinais e as lapelas abertas en calquera lugar no que use { -brand-short-name }.

## Message strings belonging to the Return to AMO flow

