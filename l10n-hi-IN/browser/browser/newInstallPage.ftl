# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### For this feature, "installation" is used to mean "this discrete download of
### Firefox" and "version" is used to mean "the specific revision number of a
### given Firefox channel". These terms are not synonymous.

title = ज़रूरी सूचना
heading = आपके { -brand-short-name } प्रोफाइल में बदलाव
changed-title = क्या बदला ?
options-title = मेरे क्या विकल्प हैं
resources = साधन
support-link = प्रोफ़ाइल मैनेजर का उपयोग कर - समर्थन लेख
sync-header = साइन इन करें या एक { -fxaccount-brand-name } खाता बनाएं
sync-label = ईमेल दर्ज करें
sync-input =
    .placeholder = ईमेल
sync-button = जारी रखे
sync-terms = आगे बढ़कर, आप <a data-l10n-name="terms"> सेवा की शर्तों </a> और <a data-l10n-name="privacy"> गोपनीयता सूचना </a> से सहमति देते हैं।
sync-learn = अधिक जानें
