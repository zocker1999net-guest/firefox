# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

cfr-doorhanger-extension-heading = अनुशंसित एक्सटेंशन
cfr-doorhanger-pintab-heading = इसे आज़माएं: पिन टैब
cfr-doorhanger-extension-sumo-link =
    .tooltiptext = मैं यह क्यों देख रहा हूँ
cfr-doorhanger-extension-cancel-button = अभी नहीं
    .accesskey = N
cfr-doorhanger-extension-ok-button = अभी जोड़ें
    .accesskey = A
cfr-doorhanger-pintab-ok-button = इस टैब को पिन करें
    .accesskey = P
cfr-doorhanger-extension-manage-settings-button = अनुशंसा सेटिंग प्रबंधित करें|
    .accesskey = म
cfr-doorhanger-extension-never-show-recommendation = मुझे यह अनुशंसा न दिखाएं
    .accesskey = स
cfr-doorhanger-extension-learn-more-link = अधिक जानें
# This string is used on a new line below the add-on name
# Variables:
#   $name (String) - Add-on author name
cfr-doorhanger-extension-author = { $name } द्वारा
# This is a notification displayed in the address bar.
# When clicked it opens a panel with a message for the user.
cfr-doorhanger-extension-notification = सिफारिश

## Add-on statistics
## These strings are used to display the total number of
## users and rating for an add-on. They are shown next to each other.

# Variables:
#   $total (Number) - The rating of the add-on from 1 to 5
cfr-doorhanger-extension-rating =
    .tooltiptext =
        { $total ->
            [one] { $total } स्टार
           *[other] { $total } स्टार्स
        }
# Variables:
#   $total (Number) - The total number of users using the add-on
cfr-doorhanger-extension-total-users =
    { $total ->
        [one] { $total } उपयोगकर्ता
       *[other] { $total } उपयोग्कत्तायें
    }

## These messages are steps on how to use the feature and are shown together.

cfr-doorhanger-pintab-step1 = उस टैब के ऊपर <b>राइट-क्लिक करें</b> जिसे आप पिन करना चाहते हैं।
cfr-doorhanger-pintab-step2 = मेन्यू से <b>पिन टैब</b> चुनें।
cfr-doorhanger-pintab-step3 = अगर साइट पर कोई अपडेट हो तो आपको पिन किए गए टैब पर एक नीला डॉट दिखाई देगा।
cfr-doorhanger-pintab-animation-pause = ठहरें
cfr-doorhanger-pintab-animation-resume = फिर से शुरू करें

## Firefox Accounts Message

cfr-doorhanger-bookmark-fxa-header = अपने बुकमार्क हर जगह सिंक करें।
cfr-doorhanger-bookmark-fxa-link-text = अभी बुकमार्क सिंक करें...
cfr-doorhanger-bookmark-fxa-close-btn-tooltip =
    .aria-label = बटन बंद करें
    .title = बंद करें

## What's New toolbar button and panel

cfr-whatsnew-button =
    .label = क्या नया है
    .tooltiptext = क्या नया है
cfr-whatsnew-panel-header = क्या नया है
