# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-3rdparty = ऐसी नीतियां बनाएं, जिससे WebExtensions chrome.storage.managed के द्वारा पहुँच सके।
policy-AppUpdateURL = मनपसंद ऐप अपडेट URL सेट करें।
policy-Authentication = जो वेबसाइटें इसका समर्थन करती हैं उनके लिए एकीकृत प्रमाणीकरण विन्यस्त करें।
policy-BlockAboutAddons = ऐड-ऑन प्रबंधक तक पहुँच को ब्लॉक करें (परिचय: ऐड-ऑन)।
policy-BlockAboutConfig = about:config पृष्ठ तक के पहुँच को अवरुद्ध करें।
policy-BlockAboutProfiles = about:profiles पृष्ठ तक के पहुँच को अवरुद्ध करें।
policy-BlockAboutSupport = about:support पृष्ठ तक के पहुँच को अवरुद्ध करें।
policy-CaptivePortal = कैप्टिव पोर्टल समर्थन को सक्षम या अक्षम करें।
policy-CertificatesDescription = प्रमाण पत्र जोड़ें या निर्मित प्रमाण पत्र का उपयोग करें।
policy-Cookies = कुकीज़ स्थापित करने के लिए वेबसाइटों को अनुमति दें या इनकार करें.
policy-DefaultDownloadDirectory = डिफ़ॉल्ट डाउनलोड निर्देशिका सेट करें।
policy-DisableAppUpdate = ब्राउज़र को अपडेट होने से रोकें‌‌‍।
policy-DisableBuiltinPDFViewer = PDF.js, { -brand-short-name } में पहले से निर्मित PDF व्यूअर को अक्षम करें.
policy-DisableDeveloperTools = डेवलपर टूल्स तक के पहुँच को ब्लॉक करें।
policy-DisableFirefoxAccounts = सिंक सहित { -fxaccount-brand-name } आधारित सेवाओं को बंद करें।
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Firefox स्क्रीनशॉट सुविधा को अक्षम करें.
policy-DisableMasterPasswordCreation = यदि सही है, तो एक मास्टर पासवर्ड नहीं बनाया जा सकता है.
policy-DisablePocket = वेबपृष्ठों को Pocket में सहेजने के लिए सुविधा को अक्षम करें।
policy-DisablePrivateBrowsing = निजी ब्राउजिंग अक्षम करें।
policy-DisableSystemAddonUpdate = सिस्टम ऐड-ऑन को इंस्टॉल करने और अपडेट करने से ब्राउज़र को रोकें।
policy-DisableTelemetry = Telemetry बंद करें।
policy-DisplayBookmarksToolbar = डिफ़ॉल्ट रूप से बुकमार्क टूलबार प्रदर्शित करें।
policy-DisplayMenuBar = डिफ़ॉल्ट रूप से मेनू बार प्रदर्शित करें।
policy-DownloadDirectory = डाउनलोड निर्देशिका सेट और लॉक करें।
policy-FirefoxHome = Firefox होम कॉन्फ़िगर करें।
policy-FlashPlugin = फ़्लैश प्लगइन के उपयोग की अनुमति दें या अस्वीकार करें।
policy-HardwareAcceleration = अगर गलत है, तो हार्डवेयर की गति को बंद करें।
# “lock” means that the user won’t be able to change this setting
policy-Homepage = सेट और वैकल्पिक रूप से मुखपृष्ठ लॉक करें।
policy-InstallAddonsPermission = कुछ वेबसाइटों को ऐड-ऑन संस्थापित करने की अनुमति दें।
policy-LocalFileLinks = विशिष्ट वेबसाइटों को स्थानीय फ़ाइलों से लिंक करने की अनुमति दें।
policy-NewTabPage = नया टैब पृष्ठ सक्षम या अक्षम करें।
policy-PopupBlocking = कुछ वेबसाइटों को डिफ़ॉल्ट रूप से पॉपअप प्रदर्शित करने की अनुमति दें।
policy-PromptForDownloadLocation = पूछें कि डाउनलोड करते समय फ़ाइलों को कहाँ सहेजना है।
policy-Proxy = प्रॉक्सी सेटिंग्स कॉन्फ़िगर करें।
policy-SearchSuggestEnabled = खोज सुझावों को सक्षम या अक्षम करें।
policy-SSLVersionMax = सबसे अधिक SSL संस्करण देखे।
policy-SSLVersionMin = सबसे कम SSL संस्करण देखे।
