# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### UI strings for the simplified onboarding modal / about:welcome
### Various strings use a non-breaking space to avoid a single dangling /
### widowed word, so test on various window sizes if you also want this.


## These button action text can be split onto multiple lines, so use explicit
## newlines in translations to control where the line break appears (e.g., to
## avoid breaking quoted text).


## Welcome modal dialog strings

onboarding-cards-dismiss =
    .title = Descartar
    .aria-label = Descartar

## Firefox Sync modal dialog strings.

onboarding-sync-welcome-header = Prene lo { -brand-product-name } con tu
onboarding-sync-welcome-content = Obtiene los tuyos marcapachinas, historials, claus y atros achustes en totz los tuyos dispositivos.
onboarding-sync-welcome-learn-more-link = Aprende mas sobre las cuentas de Firefox
onboarding-sync-legal-notice = Si contina, ye confirmando que ye d'acuerdo con as <a data-l10n-name="terms">Condicions d'uso</a> y <a data-l10n-name="privacy">Nota sobre privacidat</a>.
onboarding-sync-form-input =
    .placeholder = Correu-e
onboarding-sync-form-continue-button = Continar
onboarding-sync-form-skip-login-button = Blinca-te este paso

## This is part of the line "Enter your email to continue to Firefox Sync"

onboarding-sync-form-header = Escribe lo tuyo email
onboarding-sync-form-sub-header = pa continar enta { -sync-brand-name }.

## These are individual benefit messages shown with an image, title and
## description.


## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section


## Message strings belonging to the Return to AMO flow

