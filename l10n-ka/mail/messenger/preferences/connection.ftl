# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = მომსახურების გამოყენება
    .accesskey = რ
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (ნაგულისხმევი)
    .tooltiptext = ნაგულისხმევი URL-ბმული DNS-ის HTTPS-ით გადასაცემად
connection-dns-over-https-url-custom =
    .label = მითითებული
    .accesskey = თ
    .tooltiptext = შეიყვანეთ სასურველი URL-ბმული DNS-ის HTTPS-ით გადასაცემად
connection-dns-over-https-custom-label = მითითებული
