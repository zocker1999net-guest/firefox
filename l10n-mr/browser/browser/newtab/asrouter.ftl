# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## These messages are used as headings in the recommendation doorhanger

cfr-doorhanger-extension-heading = शिफारस केलेले विस्तार
cfr-doorhanger-pintab-heading = हे करून पहा: पिन टॅब



cfr-doorhanger-extension-sumo-link =
    .tooltiptext = मला हे का दिसत आहे
cfr-doorhanger-extension-cancel-button = आत्ता नाही
    .accesskey = N
cfr-doorhanger-extension-ok-button = आत्ताच जोडा
    .accesskey = A
cfr-doorhanger-pintab-ok-button = हा टॅब पिन करा
    .accesskey = P
cfr-doorhanger-extension-manage-settings-button = शिफारशी सेटिंग्ज व्यवस्थापित करा
    .accesskey = M
cfr-doorhanger-extension-never-show-recommendation = मला ही शिफारस दर्शवू नका
    .accesskey = S
cfr-doorhanger-extension-learn-more-link = अधिक जाणा
# This string is used on a new line below the add-on name
# Variables:
#   $name (String) - Add-on author name
cfr-doorhanger-extension-author = { $name } द्वारा
# This is a notification displayed in the address bar.
# When clicked it opens a panel with a message for the user.
cfr-doorhanger-extension-notification = शिफारस

## Add-on statistics
## These strings are used to display the total number of
## users and rating for an add-on. They are shown next to each other.

# Variables:
#   $total (Number) - The rating of the add-on from 1 to 5
cfr-doorhanger-extension-rating =
    .tooltiptext =
        { $total ->
            [one] { $total } तारा
           *[other] { $total }  तारे
        }
# Variables:
#   $total (Number) - The total number of users using the add-on
cfr-doorhanger-extension-total-users =
    { $total ->
        [one] { $total } वापरकर्ता
       *[other] { $total } वापरकर्ते
    }

## These messages are steps on how to use the feature and are shown together.

cfr-doorhanger-pintab-step2 = मेनूमधून <b>पिन टॅब</b> निवडा.
cfr-doorhanger-pintab-animation-pause = स्तब्ध करा
cfr-doorhanger-pintab-animation-resume = पुन्हा सुरू करा

## Firefox Accounts Message

cfr-doorhanger-bookmark-fxa-header = आपले बुकमार्क कुठेही सिंक करा.
cfr-doorhanger-bookmark-fxa-close-btn-tooltip =
    .aria-label = बंद करा बटण
    .title = बंद करा

## What's New toolbar button and panel

cfr-whatsnew-button =
    .label = नवीन काय आहे
    .tooltiptext = नवीन काय आहे
cfr-whatsnew-panel-header = नवीन काय आहे

## Bookmark Sync


## Send Tab

cfr-doorhanger-send-tab-header = जाता जाता हे वाचा

## Firefox Send

