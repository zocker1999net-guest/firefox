# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

appmenu-update-available =
    .label = Налично е обновяване на { -brand-shorter-name }.
    .buttonlabel = Изтегляне на обновяване
    .buttonaccesskey = и
    .secondarybuttonlabel = Не сега
    .secondarybuttonaccesskey = с
appmenu-update-available-message = Обновете { -brand-shorter-name } за повишаване на бързината и поверителността.
appmenu-update-manual =
    .label = { -brand-shorter-name } не може да бъде обновен до последното издание.
    .buttonlabel = Изтегляне на { -brand-shorter-name }
    .buttonaccesskey = И
    .secondarybuttonlabel = Не сега
    .secondarybuttonaccesskey = Н
appmenu-update-manual-message = Изтеглете ново копие на { -brand-shorter-name } и ще ви помогнем с инсталирането.
appmenu-update-whats-new =
    .value = Вижте новото в това издание.
appmenu-update-unsupported =
    .label = { -brand-shorter-name } не може да бъде обновен до последното издание.
    .buttonlabel = Научете повече
    .buttonaccesskey = П
    .secondarybuttonlabel = Затваряне
    .secondarybuttonaccesskey = З
appmenu-update-unsupported-message = Последното издание на { -brand-shorter-name } не се поддържа от вашата система.
appmenu-update-restart =
    .label = Рестарт за обновяване на { -brand-shorter-name }.
    .buttonlabel = Рестарт и възстановяване
    .buttonaccesskey = Р
    .secondarybuttonlabel = Не сега
    .secondarybuttonaccesskey = Н
appmenu-update-restart-message = След бързия рестарт { -brand-shorter-name } ще възстанови всички ваши отворени раздели и прозорци, които не са в режим на поверително разглеждане.
appmenu-addon-private-browsing-installed =
    .buttonlabel = Да, разбрах
    .buttonaccesskey = а
appmenu-addon-post-install-message = Управлявайте добавките си като изберете <image data-l10n-name='addon-install-icon'></image> от менюто <image data-l10n-name='addon-menu-icon'></image>.
appmenu-addon-post-install-incognito-checkbox =
    .label = Позволете на разширението да работи в поверителни прозорци
    .accesskey = з
appmenu-addon-private-browsing =
    .label = Променете към разширения в Поверителни прозорци
    .buttonlabel = Управление на разширения
    .buttonaccesskey = у
    .secondarybuttonlabel = Да, разбрах
    .secondarybuttonaccesskey = а
appmenu-addon-private-browsing-message = Новите разширения, които добавяте към { -brand-shorter-name } няма да работят в поверителни прозорци освен изрично не разрешите в настройките.
appmenu-addon-private-browsing-learn-more = Научете как да управлявате настройките на разширенията
