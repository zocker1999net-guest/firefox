# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Strings in this file are used to localize button titles displayed on the 
# MacBook Touch Bar.
back = Назад
forward = Напред
reload = Презареждане
home = Начало
fullscreen = Цял екран
find = Търсене
new-tab = Нов раздел
add-bookmark = Добавяне на отметка
open-bookmarks-sidebar = Показване на страничната лента с отметки
reader-view = Изглед за четене
# Meant to match the string displayed in an empty URL bar.
open-location = Търсене или въвеждане на адрес
share = Споделяне
close-window = Затваряне на прозореца
open-sidebar = Странични ленти
