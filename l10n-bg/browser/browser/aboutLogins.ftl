# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The ⋯ menu that is in the top corner of the page


## Login List

login-list-name-option = Име (A-Z)
login-list-item-subtitle-missing-username = (без потребителско име)

## Login

login-item-username =
    .placeholder = name@example.com

## Master Password notification

