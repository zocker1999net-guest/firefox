# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### For this feature, "installation" is used to mean "this discrete download of
### Firefox" and "version" is used to mean "the specific revision number of a
### given Firefox channel". These terms are not synonymous.

title = خبرهای مهم
heading = تغییرات در نمایهٔ { -brand-short-name } شما
changed-title = چه چیزی تغییر کرده؟
options-title = انتخاب‌های من چیست؟
resources = منابع:
support-link = استفاده از مدیریت نمایه - مبحث پشتیبانی
sync-header = وارد شوید یا یک { -fxaccount-brand-name } بسازید
sync-label = رایانامه خود را وارد کنید
sync-input =
    .placeholder = رایانامه
sync-button = ادامه
sync-terms = با ادامه دادن این مرحله، شما تأیید می‌کنید که با قوانین <a data-l10n-name="terms">ارائه خدمات</a> و <a data-l10n-name="privacy">حریم شخصی</a> موافق هستید.
sync-learn = اطلاعات بیشتر
